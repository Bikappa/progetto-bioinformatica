\select@language {italian}
\select@language {italian}
\contentsline {chapter}{\numberline {1}Introduzione}{3}{chapter.1}
\contentsline {section}{\numberline {1.1}La bioinformatica}{3}{section.1.1}
\contentsline {subsection}{\numberline {1.1.1}La filogenia}{3}{subsection.1.1.1}
\contentsline {section}{\numberline {1.2}L'evoluzione biologica}{4}{section.1.2}
\contentsline {section}{\numberline {1.3}Alberi filogenetici}{4}{section.1.3}
\contentsline {section}{\numberline {1.4}L\IeC {\textquoteright }analisi filogenetica}{5}{section.1.4}
\contentsline {chapter}{\numberline {2}Il progetto}{7}{chapter.2}
\contentsline {chapter}{Introduzione}{7}{chapter.2}
\contentsline {section}{\numberline {2.1}Strumenti utilizzati}{7}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Phylip}{8}{subsection.2.1.1}
\contentsline {subsubsection}{Neighbor}{8}{section*.5}
\contentsline {subsubsection}{Drawtree}{8}{section*.6}
\contentsline {subsection}{\numberline {2.1.2}Librerie software}{8}{subsection.2.1.2}
\contentsline {subsubsection}{Libreria REGEX}{8}{section*.7}
\contentsline {subsubsection}{Libreria ARGTABLE2}{8}{section*.8}
\contentsline {subsubsection}{Libreria Swing}{8}{section*.9}
\contentsline {section}{\numberline {2.2}Dataset di sequenze}{9}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Il formato FASTA}{9}{subsection.2.2.1}
\contentsline {section}{\numberline {2.3}I pattern di prosite}{9}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Sintassi dei motifs}{9}{subsection.2.3.1}
\contentsline {section}{\numberline {2.4}Valutazione dei risultati}{11}{section.2.4}
\contentsline {subsection}{\numberline {2.4.1}NCBI}{11}{subsection.2.4.1}
\contentsline {chapter}{\numberline {3}Implementazione}{14}{chapter.3}
\contentsline {section}{\numberline {3.1}Moduli software}{14}{section.3.1}
\contentsline {section}{\numberline {3.2}Metriche per il calcolo della distanza}{15}{section.3.2}
\contentsline {section}{\numberline {3.3}Pipeline dell'applicazione}{15}{section.3.3}
\contentsline {section}{\numberline {3.4}Configurazione da linea di comando}{16}{section.3.4}
\contentsline {section}{\numberline {3.5}Gui}{16}{section.3.5}
\contentsline {chapter}{\numberline {4}Testing}{19}{chapter.4}
\contentsline {chapter}{\numberline {5}Conclusioni}{26}{chapter.5}
\contentsline {section}{\numberline {5.1}Performance}{26}{section.5.1}
\contentsline {section}{\numberline {5.2}Difficolt\IeC {\`a} riscontrate}{27}{section.5.2}
\contentsline {section}{\numberline {5.3}Lezioni apprese}{27}{section.5.3}
\contentsline {section}{\numberline {5.4}Sviluppi futuri}{27}{section.5.4}
\contentsline {chapter}{Appendices}{28}{section*.25}
\contentsline {chapter}{\numberline {A}Conversione pattern di prosite in espressione regolare}{29}{Appendice.1.Alph1}
