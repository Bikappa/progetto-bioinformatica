	\documentclass[a4paper,10pt,titlepage, openright]{report}


\usepackage[T1]{fontenc}
\usepackage[italian]{babel}
\usepackage[utf8]{inputenc}
\usepackage{fancyhdr}
\usepackage{frontespizio}
\usepackage{hyperref}
\usepackage{rotating}
\usepackage[numbers]{natbib}
\usepackage{notoccite}
\usepackage{verbatim}
\usepackage{url}
\usepackage{epstopdf}
\usepackage{graphicx}
\usepackage{svg}
\usepackage{url}

\usepackage[toc,page]{appendix}

\begin{document}

\begin{titlepage}

\begin{frontespizio}
\Universita{Padova}
\Logo[4.5cm]{logo}
\Divisione{Scuola di Ingegneria}
\Corso{Ingegneria informatica}
\Annoaccademico{2014--2015}
\Titoletto{Algoritmi per la Bioinformatica	\\ relazione di progetto }
\Titolo{Costruzione di alberi filogenetici}
\Sottotitolo{Implementazione di un tool per l'analisi filogenetica basata su segnali biologici e sperimentazione su dati reali.}

\NCandidati{Studenti}

\Candidato[1109414]{Luca Bianconi}
\Candidato[1111244]{Xiaobin Duan}

\NRelatore{Docente}{Docenti}

\Relatore{Prof.ssa Cinzia Pizzi}
\Relatore{Prof. Matteo Comin}

\Margini{3cm}{3.5cm}{3cm}{2cm}
\end{frontespizio}
\end{titlepage}
\normalsize

\tableofcontents
\listoffigures

\chapter[Introduzione]{Introduzione}

\section[La bioinformatica]{La bioinformatica}
La bioinformatica è una disciplina scientifica dedicata alla risoluzione di
problemi biologici a livello molecolare con metodi informatici. 
Essa costituisce un tentativo di descrivere dal punto di vista numerico e 
statistico i fenomeni biologici. \\ 
La biologia storicamente ha avuto un minor approccio matematico rispetto ad
altre scienze e la bioinformatica tenta di supplire a questa lacuna fornendo ai risultati
tipici della biochimica e della biologia molecolare un corredo di strumenti
analitici e numerici\cite{bioinformatichistory}. \\ 
Vengono coinvolte, oltre all'informatica, discipline come matematica
applicata, statistica, chimica, biochimica e nozioni di intelligenza
artificiale.\\
	
Gli obbiettivi della bioinformatica sono principalmente:
\begin{itemize}
\item fornire modelli statistici validi per l'interpretazione dei dati
provenienti da esperimenti di biologia molecolare e biochimica al fine di 
identificare tendenze e leggi numeriche.

\item Generare nuovi modelli e strumenti matematici per l'analisi di sequenze
 di DNA, RNA e proteine al fine di creare un insieme di conoscenze relative alla
 frequenza di sequenze rilevanti, la loro evoluzione e funzione.

\item Organizzare le conoscenze acquisite a livello globale
all'interno di database, al fine di rendere tali informazioni accessibili a
tutti, e ottimizzare gli algoritmi di ricerca dei dati stessi per migliorarne l'accessibilità.
\end{itemize}

Gli attuali ambiti di ricerca includono l’allineamento di sequenze, la
predizione genetica, la predizione di struttura proteica, l’espressione genica
e l’interazione proteina-proteina.

\subsection{La filogenia}
Filogenesi, o filogenia, è lo studio dell’evoluzione e delle relazioni fra
gruppi, specie o popolazioni,  di organismi\cite{biologyonline}.  Studio
effettuato con l’analisi e il confronto di strutture proteiche o sequenze
nucleotidiche dalle quali si ricavano matrici di dati su cui poter effettuare diverse operazioni.

I risultati del processo sono ipotesi sulla storia evolutiva dei gruppi
tassonomici  e la loro filogenia. I risultati vengono spesso mostrati sotto
forma di alberi filogenetici.

\section[L'evoluzione biologica]{L'evoluzione}
L'evoluzione biologica consiste nell'insieme di cambiamenti dei tratti
ereditabili di una popolazione di organismi nel susseguirsi di diverse generazioni. Si parla di
evoluzione da parte di popolazioni di organismi e non del singolo individuo.

Si distinguono due tipi di evoluzione in base alla durata dei cambiamenti biologici e alla loro natura:
\begin{itemize}
\item \textbf{Macro-evoluzione}: evoluzione su larga scala che avviene in tempi
geolocici e porta alla formazione di nuove specie e nuovi gruppi tassonomici,
detti anche \textit{taxa}.
\item \textbf{Micro-evoluzione}: cambiamenti dei tratti ereditabili di un gruppo
di individui all'interno di una specie che non porta alla formazione di nuove specie.
\end{itemize}

L'idea alla base dell'evoluzione è quindi che ogni specie sulla terra condivida
un antenato  comune con altre specie. Attraverso il processo di discendenza con
mutazioni l'antenato comune ha dato vita nel tempo alla diversità di specie osservabile oggi.
Questo processo è chiamato speciazione.

\section[Alberi filogenetici]{Alberi filogenetici}
Gli alberi filogenetici sono lo strumento usato per descrivere visivamente il
processo evolutivo  di un insieme di specie o gruppi tassonomici permettendo di
osservarne le  relazioni e gli antenati comuni.\\

Un albero filogenetico, figura \ref{fig:albero_filogenetioc}, è costituito da
nodi e rami. Un nodo rappresenta una specie o gruppo tassonomico mentre i rami collegano due nodi fra loro
descrivendo così una relazione evolutiva: un collegamento fra due specie. Se due
nodi, A e B, sono collegati da rami ad uno stesso nodo C questo rappresenta
“l’antenato comune” delle specie descritte dai due nodi, A e B di partenza: si
dice che A e B discendono  dall'ancestore C.\\
I nodi con solo un ramo associato sono detti "foglie", o nodi esterni. Le foglie
rappresentano le specie di cui si vuole studiare il processo evolutivo.\\

L'albero filogenetico descrive quindi gli eventi di speciazione che si sono
susseguiti nel tempo creando nuove specie. La lunghezza di un ramo misura la
divergenza evolutiva tra i due nodi che connette: più lungo è il ramo, maggiore
è l’evoluzione intercorsa,  e quindi la differenza biologica, tra l’antenato e
il nodo discendente. Una specie S1 assomiglia, dal punto di vista biologico,
maggiormente ad una specie S2 rispetto ad una  specie S3 se condivide con S2 un
antenato che non condivide con S3, si veda la figura \ref{fig:somiglianza}.

Si possono distinguere due tipologie di albero filogenetico:
\begin{itemize}

\item Albero con radice, \textit{rooted}, possiede un nodo particolare, detto
appunto radice,  che rappresenta il comune ancestore di tutti i nodi
rappresentati nell'albero. Viene rappresentato tramite un grafo diretto ed è
possibile individuare sempre il nodo  ancestore di un'altro. In questo modo, un
albero \textit{rooted} è in grado  di fornire informazioni sia sulla correlazione
genetica esistente tra gli organismi presenti sulle sue ramificazioni, sia sui 
rapporti evolutivi che intercorrono tra gli stessi.

\item Un albero senza radice, \textit{unrooted}, descrive esclusivamente le relazioni genetiche tra le unità tassonomiche senza fornire alcuna informazione circa il processo evolutivo. In altre parole, indica soltanto quanto una specie è lontana da un’altra in termini genetici.

\end{itemize}

\begin{figure}[!ht]
\begin{center}
\includegraphics[scale=0.5]{images/tree.png}
\caption{Esempio di albero filogenetico} \label{fig:albero_filogenetioc}
\end{center}
\end{figure}

\begin{figure}[!ht]
\begin{center}

\includegraphics[scale=0.5]{images/fig2.png}
\caption{Somiglianze di due specie con un antenato
comune}\label{fig:somiglianza}
\end{center}
\end{figure} 

\section[L’analisi filogenetica]{L’analisi filogenetica}

L'analisi filogenetica ha lo scopo di creare, partendo da sequenze di materiale genetico, un albero filogenetico che le rappresenti.

L'analisi si basa sulle seguenti ipotesi:
\begin{enumerate}
\item Le sequenze genetiche sono corrette e originate dalla sorgente specificata.
\item Le sequenze sono omologhe (tutte discendono in qualche modo da un antenato comune).
\item Ogni posizione in un allineamento di sequenze è omologa con le altre.
\item Ognuna delle sequenze ha una storia filogenetica in comune con le altre (non sono mischiate sequenze di diverso tipo).
\item Il campione di taxa è sufficiente per risolvere il problema di interesse.
\item Le variazione nelle sequenze sono significative.
\end{enumerate}
I passi principali dell'analisi sono:
\begin{enumerate}
\item Allineamento, costruzione modello di dati, estrazione dataset. 
\item Determinazione del modello di sostituzione.
\item Costruzione dell'albero.
\item Valutazione della qualità dell'albero.
\end{enumerate}

La trattazione dettagliata dei passi esula dallo scopo di questa
relazione\footnote{Il lettore interessato può trovare maggiori informazioni all'indirizzo \url{http://www.bioon.com/book/biology/bioinformatics/chapter-14.pdf}.}.

\begin{comment}
\textbf{Passo 1:}  \\\\
la costruzione del dataset è un problema complesso in quanto occorre scegliere se analizzare le sequenze geniche o le sequenze proteiche.Le prime contengono informazioni dettagliata ma rumorose adatte a trovare relazioni tra organismi ”evolutivamente” vicini, invece le sequenze proteiche contengono informazioni grezze ma pulite adatte a trovare le relazioni generali\\\\\textbf{Passo 2:}\\\\Alcuni metodi di ricostruzioni filogenetiche sono basati:
\begin{itemize}
\item sul DNA.
\item sulla stima di distanze.
\item sulla parsimonia.
\item sulla verosimiglianza.
\end{itemize}
L’uso di questi metodi di ricostruzioni comportano sia dei vantaggi sia dei svantaggi ad esempio alcuni vantaggi nell’ uso del primo metodo sono :
\begin{enumerate}
\item Descrizione dei caratteri non ambigua.
\item Somiglianza dovuta a effetti ambientali non genetici non interferisce.
\item Evoluzione convergente implica spesso fenotipi simili ma genotipi differenti.
\item Maggiore facilita’ di stimare tempi di divergenza.
\item Modelli statistici rigorosi.
\item Tutti gli individui hanno DNA
\end{enumerate}
e alcuni svantaggi:
\begin{enumerate}
\item Mutazioni ricorrenti modificano la relazione tra distanza genetica e distanaza temporale.
\item Duplicazioni e trasferimenti orizzontale di geni possono essere identificati,ma possono creare problemi nella ricostruzione filogenetica.
\item I modelli di evoluzione del DNA possono essere molto complessi.
\item Alberi di geni e alberi di specie possono essere diversi.
\end{enumerate}
Invece i vantaggi basati sulla stima di distanze sono :
\begin{enumerate}
\item Veloce: adatto per analizzare grandi data sets.
\item Dispone solo della matrice delle distanze.
\end{enumerate}
e gli svantaggi:
\begin{enumerate}
\item Netta perdita di informazione : dalle distanze non si torna indietro alle sequenze.
\item Problemi con misure di distanze non lineari con il tempo.
\end{enumerate}
Per questo progetto si è scelto il metodo della ricostruzione filogenetica basato sulla stima delle distanze.\\\\ \textbf{Passo 3:}\\\\Una volta utilizzato uno dei metodi per la ricostuzione filogenetica  si passa alla costruzione del albero filogenetico. Nel nostro caso una volta ottenuto la matrice delle distanze ,esso viene passato  al metodo Neighbor contenuto nel package PHYLIP per costruire l’ albero in formato Newick
 \\\\\textbf{Passo 4:}\\\\Per valutare la qualità dell’ albero ottenuto è necessario avere  un albero di riferimento, per questo progetto si ottiene dalla tassonomia di NCBI.
\end{comment}


\chapter{Il progetto} \label{progetto}
\addcontentsline{toc}{chapter}{Introduzione}
Lo scopo del progetto è l’implementazione di un tool software efficiente per la
costruzione  di alberi filogenetici utilizzando, come fattore per il calcolo
della distanza  fra diverse sequenze di DNA, il numero di occorrenze dei pattern
al loro interno.

Si è voluto quindi andare a testare una strategia differente dai metodi
tradizionali: basati, per il calcolo della distanza fra sequenze, 
sulla posizione con cui un pattern compare all'interno delle sequenze stesse.

In questa sezione verranno illustrate le scelte effettuate e gli strumenti utilizzati per l'implementazione del software.

\section{Strumenti utilizzati}

Per lo sviluppo del tool sono stati utilizzati i seguenti strumenti:
\begin{itemize}
\item sofware:

\begin{itemize}
\item sistema operativo Ubuntu 14.04 LTS,
\item GNU \textit{gcc} compilatore per il linguaggio di programmazione C,
\item Java SDK versione 1.8 per la compilazione del codice Java,
\item pacchetto di software \textit{phylip},
\item libreria C \textit{argtable2} per il passaggio e il parsing di parametri
da riga di comando,
\item libreria C \textit{regex},
\item libreria Java \textit{swing} per la realizzazione dell'interfaccia grafica,
\item Eclipse IDE per lo sviluppo del codice in linguaggio C e Java,
\item Git come software per il controllo di versione.
\end{itemize}

\item hardware:
\begin{itemize}
\item notebook con processore Intel I7, quad-core da 2.4GHz.
\end{itemize}

\end{itemize}

\subsection{Phylip}
Phylip è un pacchetto software completo per l'analisi filogenetica creato da
Joseph Felsenstein all'Università di Washington. Questo pacchetto contiene molti
degli algoritmi discussi nella letteratura della bioinformatica\cite{phylip}.

Phylip è ottenibile gratuitamente all'indirizzo \url{http://evolution.genetics.washington.edu/phylip.html}.

\subsubsection{Neighbor}
Neighbor è un programma del pacchetto Phylip che implementa il metodo
Neighbor-Joining di Saitou e Nei (1987) e il metodo di clustering
UPGMA\footnote{Unweighted Pair Group Method with Arithmetic Mean}.

Neighbor costruisce un albero \textit{unrooted} a partire da una matrice di
distanza fornita. Un punto di forza del programma è la velocità di elaborazione.

\subsubsection{Drawtree}
Un altro programma del pacchetto Phylip è drawtree il quale prende in ingresso
un albero scritto su file in formato
newick\footnote{http://evolution.genetics.washington.edu/phylip/newicktree.html}
e ne traccia il un grafico scrivendolo su un file in output.
\`E stato usato il codice sorgente di drawtree per disegnare il grafico
dell'albero generato in formato postscript.

\subsection{Librerie software}
\subsubsection[Libreria REGEX]{Regex}
\label{regex}
La libreria \textit{regex.h} fa parte della libreria open-source \textit{GNU C}
ed è presente,  \textit{built-in}, nella maggioranza dei sistemi basati su
\textit{UNIX}. Essa consente di effettuare tutte le operazioni più comuni con le
espressioni regolari scritte nella sintassi standard \textit{POSIX}.

Si è scelto di usare questa libreria per la comprovata solidità e efficienza
derivate  da anni di utilizzo, revisione e aggiornamento da parte degli
sviluppatori  linux. Questo per avere uno strumento efficace ed efficiente per
effettuare  il macthing di pattern all'interno di stringhe.

\subsubsection[Libreria ARGTABLE2]{Argtable2}
La libreria C \textit{argtable2} consente di gestire i parametri passati ad un 
programma da linea di comando in maniera organizzata.
Si è utilizzata questa libreria nell'implementazione l'implementazione del tool
per fare in modo che il programma possa ricevere in input gli argomenti per la
sua esecuzione, in maniera \textit{linux-friendly}, e si comporti di
conseguenza.

\subsubsection[Libreria Swing]{Swing}
Swing è una libreria Java che contiene un vasto assortimento di strumenti per la
realizzazione di interfacce grafiche. \`E stata utilizzata per realizzare la
parte l'interfaccia utente del progetto descritta nella sezione \ref{gui};

\section[Dataset di sequenze]{Dataset di sequenze}

Il dataset di sequenze mitocondriali utilizzato per i test seguenti è mostrato
in figura \ref{dataset}.

Tutte le sequenze sono state scritte in formato FASTA in un unico file
descritto brevemente nella prossima sezione.

\subsection{Il formato FASTA}
Il formato file è uno dei più antichi formati per la rappresentazione di
sequenze genomiche. \\ 
Prevede che ogni sequenza venga preceduta da una unica riga di descrizione,
detta \textit{header}, preceduta dal carattere '>' (simbolo "maggiore di"). La
sequenza è invece rappresentata da una serie di righe, non più lunghe di 120 caratteri, che
seguono l'\textit{header}. Solitamente però si preferisce avere righe più corte,
di 80 caratteri o meno, per una miglior lettura e visualizzazione\cite{fasta}.
\\
Il formato prevede anche la possibilità di inserire commenti, righe precedute
dal carattere ';' (punto e virgola), che vengono ignorati in fase di esecuzione
ma questi non sono utilizzati spesso; si preferisce inserire qualsiasi
descrizione nella riga \textit{header}. \\
La riga \textit{header} ha formato libero anche se è ne stato pensato uno
adottato da molti dei principali siti di biologia, il formato
UniProtKB\cite{uniprot}.\\
Un \textit{header} UniProtKB è scritto con la seguente struttura:\\
\begingroup
    \fontsize{6pt}{8pt}\selectfont
    \begin{verbatim}  
        >db|UniqueIdentifier|EntryName ProteinName OS=OrganismName[GN=GeneName]PE=ProteinExistence SV=SequenceVersion
    \end{verbatim}
\endgroup

Nello sviluppo del tool si è assunto che l'\textit{header} sia nel formato sopra
indicato. In particolare il software ricerca nella riga la sottostringa
doppi apici  \textquotedblleft OS=\textquotedblright\ che precede il nome
dell'organismo associato alla sequenza.
\\
Si è inoltre assunto che il file non contenga commenti.


\begin{figure}[h!]
\begin{center}
\includegraphics[scale=1]{images/fig3.png}
\caption{Elenco sequenze mitocondriali} \label{dataset}
\end{center}
\end{figure} 
\section[I pattern di prosite]{I pattern di prosite}
I pattern, o motif, utilizzati nella ricerca nel progetto sono stati ottenuti 
dal sito \href{http://prosite.expasy.org/}{PROSITE}.
Si tratta di un insieme di 1308 \textit{motifs} che si andranno a cercare
all'interno  delle sequenze proteiche del dataset.

\subsection{Sintassi dei motifs}
La sintassi dei motif in PROSITE è la seguente \cite{prosite}:
\begin{itemize}
\item Viene utilizzato il codice uniletterale standard IUPAC per gli aminoacidi.
\item Il simbolo 'x' è usato per le posizioni dove ogni aminoacido è accettato.
\item Le posizioni ambigue sono indicate elencado gli aminoacidi accettati fra parentesi quadre '[ ]'.
\item Le posizione ambigue sono anche indicate elencando gli aminoacidi che non sono ammessi in una posizione fra parentesi graffe '{ }'.
\item Ogni elemento di un pattern è separato dal vicino da un carattere '-'.
\item Ripetizioni di un elemento possono essere indicate facendo seguire l'elemento da un valore numerico o un range di valori numerici fra parentesi tonde. 

Esempi:\\
x(3) corrisponde a x-x-x\\
x(2,4) corrisponde a x-x o x-x-x o x-x-x-x
\item Quando un pattern è ristretto a un N- o C- terminale di una sequenza
questo pattern inizia rispettivamente con un simbolo '<' o termina con un simbolo '>'.
\end{itemize}
 
Un esempio di pattern è il seguente:
\begin{verbatim}
F-[GSTV]-P-R-L-G
\end{verbatim}
Come si può notare la sintassi dei pattern di PROSITE non è standard e tantomeno
non è nello  standard POSIX su cui si pasa la libreria regex.h che si è deciso
di  usare per il progetto come indicato nella sezione \ref{regex}.

Pertanto è stato necessario implementare un algoritmo che convertisse un motifs
scritto nel formato di PROSITE in un'espressione regolare in formato POSIX.
Il codice viene riportato nell'appendice \ref{prositetoposix}.

\section{Valutazione dei risultati}
Per valutare i risultati ottenuti, ovvero gli alberi generati dal programma, è
stato usato come riferimento un albero ricavato dal sito
di NCBI\footnote{\url{http://www.ncbi.nlm.nih.gov/taxonomy}} partendo,
ovviamente, dallo stesso sottoinsieme di specie del nostro dataset.\\
Prima di poter confrontare questo albero con quelli prodotti dal tool è stato
necessario convertirlo in un albero binario.

\subsection{NCBI}
NCBI, acronimo di \textquotedblleft National Center for Biotechnology
Information\textquotedblright\ è l'ente che mantiene, dagli inizi degli anni
novanta, il database \textit{GenBank} precedentemente costruito dal  Los Alamos
National Laboratory (LANL).\\
Il database \textit{GenBank} è una collezione di tutte le sequenze nucleotidiche
pubbliche e le loro sintesi proteiche. É stato creato dalla collaborazione con
l'European Molecular Biology  Laboratory (EMBL), dall'archivio dati
dell'European Bioinformatics Institute (EBI) e dalla DNA Data Bank of Japan
(DDBJ).\\
Il database riceve sequenze genomiche dai laboratori di tutto il mondo per più
di 100.000 organismi e continua a crescere con ritmo esponenziale, raddoppiando
dimensioni ogni dieci mesi.

\begin{figure}[h!]
\centering
\includegraphics[scale=0.25]{images/ncbi.png}
\hspace{3mm}
\caption{Sito NCBI}
\end{figure} 

\begin{figure}[!ht]
\begin{center}
\includegraphics[scale=1]{images/TreeNCBI.png}
\caption{Albero di riferimento}
\end{center}
\end{figure} 

\chapter{Implementazione}
In questo capitolo verranno discusse le scelte implementative e la
struttura del software creato.\\
Il tool è stato realizzato in linguaggio C, sviluppato e testato per il sistema
operativo Linux, distribuzione Ubuntu. Non è previsto il funzionamento in altri
sistemi operativi.\\
Sono stati realizzati due programmi: il primo, denominato \textit{core}, è un
programma con interfaccia da riga di comando e svolge tutte le funzioni logiche
descritte in seguito, mentre il secondo, chiamato \textit{gui}, è un'interfaccia
grafica, realizzata in linguaggio Java, che permette di configurare i parametri
del tool in maniera user-friendly.

\section{Moduli software}
Il codice è stato organizzato in diversi moduli per rispecchiarne le diverse
funzionalità. Questo lo rende più leggibile e meglio mantenibile.\\

\begin{itemize}
  \item \textbf{run\_config} si preoccupa di gestire la parte di codice dedicata
  alla lettura e impostazione dei parametri passati da linea di comando. Per far ciò
  si avvale delle funzioni messe a dispozione dalla libreria argtable2.
  Nell'header run\_config.h si definiscono i valori di default che i parametri
  devono assumere se non venissero indicati dall'utente. Viene inoltre definita
  una struttura dati, chiamata \textit{RunConfig}, dedicata a contenere le
  informazioni necessarie alla corretta esecuzione del programma secondo i
  parametri impostati. \\
  Una istanza di \textit{RunConfig} viene creata all'inizio dell'esecuzione e passata
  successivamente a tutte le funzioni che devono cambiare il loro comportamento
  a seconda dei parametri decisi dall'utente.
  \item \textbf{motif\_reader} comprende il codice per la lettura del file dei
  pattern di input. Dalla scansione del file viene creata una \textit{linked list} che al
  termine della procedura viene convertita in un array.\\
  I motif letti vengono riscritti in espressioni regolari e si creano le
  relative strutture \textit{regex\_t} (libreria argtable2).
  \item \textbf{motif\_to\_regex} contiene la funzione che effettua la
  conversione di una stringa pattern di prosite a una stringa espressione regolare in formato
  POSIX.
  \item \textbf{sequence\_scanner} effettua il matching di un array di
  espressioni regolari con le sequenze in formato fasta contenute in un file di input. Per
  ogni sequenza viene creato un vettore che contiene per ogni pattern il numero
  di occorrenze all'interno della sequenza.\\
  Questi vettori vengono infine riuniti in un'unica matrice, chiamata matrice
  delle frequenze.
  \item \textbf{fasta\_file\_parser} si preoccupa della lettura del file di
  sequenze in input: le sequenze vengono lette una ad una quando richiesto all'interno del
  codice contenuto in sequence\_scanner
  \item \textbf{metric implementa} i diversi algoritmi per i metodi di calcolo
  della distanza fra due vettori. Ogni funzione prende in ingresso una matrice
  delle frequenze e genera un'altra matrice in cui ogni elemento rappresenta la
  distanza calcolata fra la specie associata alla colonna e quella associata alla riga.\\
  Le metriche implementate sono riportate nella sezione \ref{metriche}.\\
  \item \textbf{tree} crea i file in output che rappresentano l'albero
  filogenetico data una matrice delle distanze.
  \item \textbf{plot} effettua il rendering del grafico di un albero.
\end{itemize}

\section{Metriche per il calcolo della distanza}
Le metriche usate nel programma, all'interno del modulo \textit{metric}, per
calcolare la distanza fra due vettori \textit{X} e \textit{Y} sono:
\begin{itemize}
\item Distanza euclidea\\
  \begin{equation}
    d(X,Y) = \sqrt{\sum_{i=1}^N \left( X_i-Y_i \right)^2}
  \end{equation}
  
  \item Distanza chi-square:\\
  \begin{equation}
    d(X,Y) = \frac{\sum_{i=1}^N \frac{\left(X_i-Y_i \right)^2}{X_i+Y_i}}{2}
  \end{equation}
  
\item Distanza Jaccard:\\
  \begin{equation}
    J(X,Y) = 1- \frac{\mid X \cap Y \mid}{\mid X \cup Y \mid}
  \end{equation}

\end{itemize}

\section{Pipeline dell'applicazione}
La pipeline, il flusso, dell'applicazione è illustrato nella figura
\ref{pipeline}. \\

\begin{figure}[!ht]
\centering
\includegraphics[width=\textwidth]{images/pipeline.pdf}
\caption{Pipeline dell'applicazione} \label{pipeline}
\end{figure}

\section{Configurazione da linea di comando}

Per lanciare l'esecuzione del programma \textit{core} bisogna aprire una
finestra del terminale e digitare:
\begin{verbatim}
 ./pbcore [lista dei parametri separati da spazi]
\end{verbatim}
dove le parentesi quadre stanno ad indicare che i parametri sono facoltativi, se
non sono esplicitamente indicati verranno usati quelli predefiniti.\\
I parametri possibili sono mostrati nella tabella \ref{table:parametri}.


\begin{table}
\begin{center}
\tiny

\begin{tabular}{|l|p{1.5cm}|p{0.7cm}|p{1cm}|p{3cm}|p{3cm}|p{3cm}|}
\hline
\textbf{\#} & \textbf{Parametro} & \textbf{Forma corta} & \textbf{Tipo} &
\textbf{Descrizione} & \textbf{Possibili valori} & \textbf{Esempio} \\

\hline
1 & --sequences &  & file & percorso del file delle sequenze & qualsiasi
percorso valido & --sequences=myseqs.txt\\
\hline
2 & --motifs &  & file & percorso del file dei motif & qualsiasi percorso
valido & --motifs=mymots.txt\\
\hline
3 & --metric & -m & stringa & metrica di calcolo della distanza & euclidean,
chi-square, jaccard & --metric=euclidean\\
\hline
4 & --newicktree &  & file & percorso del file dove andrà salvato l'albero
scritto in formato newick & qualsiasi percorso valido & --newick=tree.newick\\
\hline
5 & --tree &  & file & percorso del file dove andrà salvato l'albero scritto
in formato testuale & qualsiasi percorso valido & --tree=tree.txt\\
\hline
6 & --plot &  & file & percorso del file dove andrà salvato il grafico
dell'albero in formato postscript & qualsiasi percorso valido & --plot=grafico.ps\\
\hline
7 &  & -s & boolean & indica, se presente, di non produrre il grafico in output
 & & -s\\
\hline
\end{tabular}
\end{center}

\begin{center}
\begin{tabular}{|l|l|}
\hline
\textbf{\#} & \textbf{Valore predefinito}\\
\hline
1 & sequences.fasta\\
\hline
2 & motifs.txt\\
\hline
3 & euclidean\\
\hline
4 & outtree.newick\\
\hline
5 & outtree.txt\\
\hline
6 & plot.ps\\
\hline
7 &  non impostato, il grafico viene creato\\
\hline
\end{tabular}
\caption{Parametri da linea di comando} \label{table:parametri}
\end{center}
\end{table}

Per esempio il comando

\begin{verbatim}
 ./pbcore --sequences=seqs.txt --motif=mots.txt -m euclidean
 --newicktree=unreadabletree.txt -s
\end{verbatim}

userà i file ``seqs.txt'' e ``mots.txt'' in input, calcolerà la distanza con la
metrica euclidea, produrrà in output i file ``unreadabletree.txt'' e
``outtree.txt'' e nessun grafico.


\section{Gui}\label{gui}
La finestra dell'interfaccia grafica del programma è visibile in figura
\ref{img:gui}.\\
Presenta due campi per la selezione dei file di ingresso, sequenze e motif, tre
pulsanti per scegliere la metrica di calcolo della distanza fra le
specie e tre campi per i file di output. È presente un \textit{checkbox} per
evitare la scrittura del grafico dell'albero oltre, ovviamente, ad un tasto
per avviare l'elaborazione.\\
Infine nella parte destra della finestra viene mostrato l'output testuale,
\textit{log} del programma \textit{core} una volta avviato dall'interfaccia.

\begin{figure}[!ht]
\centering
\includegraphics[scale=0.3]{images/gui.png}
\caption{Interfaccia utente del programma} \label{img:gui}
\end{figure} 

\chapter[Testing]{Risultati Test}

Di seguito vengono riportati i grafici degli alberi generati a partire dai dati
descritti nel capitolo \ref{progetto} con le diverse metriche di distanza.\\
In figura \ref{fig:albero_riferimento} è mostrato l'albero di riferimento
utilizzato per il confronto dei risultati.\\
Successivamente sono riportati alcuni grafici sulle performance del programma.\\
\\

\begin{figure}[h]
\centering

\makebox[\textwidth][c]{\includegraphics[trim=0.5cm 7cm 0.5cm
7cm, width=1.3\textwidth]{images/euclidean.pdf}}
\caption{Albero con metrica euclidean}\label{euclidean}
\end{figure} 

\begin{figure}[!ht]
\centering
\makebox[\textwidth][c]{\includegraphics[width=1.3\textwidth]{images/chisquare.pdf}}
\caption{Albero con metrica chi-square}
\end{figure}

\begin{figure}[h]
\centering
\makebox[\textwidth][c]{\includegraphics[width=1.3\textwidth]{images/jaccard.pdf}}
\caption{Albero con metrica jaccard}\label{jaccard}
\end{figure} 


\begin{figure}[h]
\centering
\makebox[\textwidth][c]{\includegraphics[width=1.3\textwidth]{images/ncbitree.pdf}}
\caption{Albero di riferimento}\label{fig:albero_riferimento}
\end{figure}



\begin{figure}[h]
\centering

 \makebox[\textwidth][c]{\includegraphics[width=1.4\textwidth]{images/chart1.png}}
\caption{Performance in funzione della lunghezza delle
sequenze (input piccoli)}\label{fig:chart1}
\end{figure}

\begin{figure}[h]
\centering
 \makebox[\textwidth][c]{\includegraphics[width=1.4\textwidth]{images/chart2.png}}
\caption{Performance in funzione del numero di motif}\label{fig:chart2}
\end{figure}

\begin{sidewaysfigure}[ht]
\centering
\includegraphics[height=0.7\textwidth, width=\textwidth]{images/chart3.png}
\caption{Benchmark}\label{fig:chart3}
\end{sidewaysfigure}

\begin{figure}[h]
\centering
 \makebox[\textwidth][c]{\includegraphics[width=1.4\textwidth]{images/chart4.png}}
\caption{Benchmark (input piccoli)}\label{fig:chart4}
\end{figure}

\begin{figure}[h]
\centering
 \makebox[\textwidth][c]{\includegraphics[width=1.4\textwidth]{images/chart5.png}}
\caption{Comparativa tempo di matching e tempo di esecuzione}\label{fig:chart5}
\end{figure}

\chapter{Conclusioni}

Confrontando gli alberi ottenuti dal tool sviluppato con  l’albero di
riferimento di NCBI si nota che:

\begin{itemize}
  \item l'albero prodotto dalla metrica jaccard non contiene informazione utile,
  tutte le specie appaiono in sostanza equidistanti fra loro. Questo può essere
  dovuto alla poca sensibilità del metodo che si basa sulla presenza o meno di
  un pattern nelle sequenze e non sul numero di occorrenze.
  \item i metodi chi-square e euclidean producono alberi comparabili ma con
  differenze locali importanti. Per esempio si osservi il sottoalbero che
  contiene le specie gorilla e homo sapiens, associati allo stesso ancestore in
  entrambi gli alberi: questo sottoalbero è collegato dalla metrica chi-square
  alla specie mus musculus mentre dalla metrica euclidea alla specie cavia
  porcellus.
  \item l'albero di riferimento presenta sostanziali differenze con quelli
  prodotti da qualsiasi metrica basata sul numero di occorrenze. A questo
  proposito si non che lo stesso sottoalbero portato in esempio precedentemente
  non è collegato direttamente ne alla specie cavia porcellus ne alla specie mus
  musculus. Anzi queste due specie risultano, nell'albero di riferimento,
  piuttosto distanti dalle specie gorilla e homo sapiens.
\end{itemize}

I risultati ottenuti fanno pensare che calcolare la distanza fra sequenze
genetiche  basandosi sul numero di occorrenze di motifs al loro interno non sia
opportuna, almeno non in riferimento all'albero di NCBI.


\section{Performance}
Le velocità di esecuzione del programma sul test set usato per il progetto sono
state più che soddisfacenti. L'esecuzione termina infatti in meno di due
secondi.\\
Andando ad analizzare i grafici per diverse moli di dati si può notare come in
una prima fase, per numeri di sequenze relativamente bassi, il tempo di
esecuzione sia dominato dal tempo di matching. L'algoritmo di matching ha
complessità all'incirca $O(ls*nm)$ dove $ls$ indica la lunghezza totale delle
sequenze e $nm$ il numero di motif. Si noti che in realtà la complessità dipende
dall'informazione contenuta nei motif ovvero il pattern da cercare all'interno
delle sequenze. Tuttavia per motif di numero sufficientemente grande e omogenei
fra loro la complessità sopra indicata si rivela valida.\\
Quando il numero di sequenze diventa sufficientemente alto la complessità della
fase di calcolo delle distanze, $O(n_s^2)$ con $n_s$ numero di sequenze, e
dell'algoritmo neighbor-joining, $O(n_s^3)$, diventano prominenti.\\ In
definitiva l'algoritmo ha tempo di esecuzione $O(ls*nm)$ finchè $ls * nm
>> n_s^2$ altrimenti ha tempo di esecuzione $O(n_s^3)$.

\section{Difficoltà riscontrate}
Durante lo sviluppo del tool la maggiore difficoltà riscontrata è stata la
comprensione del codice sorgente del software PHYLIP, non documentato, non
commentato e di difficile lettura.\\
La scelta di scrivere il programma in linguaggio C per poter includere il codice
di PHYLIP ha allungato i tempi del progetto.

\section{Lezioni apprese}
Nello sviluppo del progetto si è imparato ad usare strumenti e conoscenze della
biologia. Si sono sviluppate migliori abilità nella programmazione in linguaggio
C e nell'uso del software di \textit{versioning} Git.\\
Inoltre sono state studiate le librerie \textit{argtable2} e \textit{regex} per
poter essre utilizzate nel software creato.

\section{Sviluppi futuri}
Un possibile sviluppo futuro potrebbe essere un “raffinamento”
degli  algoritmi usati: per esempio associando un “peso” ai diversi motif, in
modo da dare un punteggio maggiore a quelli ritenuti più significativi.\\
Si potrebbe inoltre pensare ad un algoritmo “ibrido” che metta in relazione in
qualche modo sia il numero delle occorrenze che la posizione in cui queste appaiono
all'interno delle sequenze.\\
Il parsing delle sequenze dai file in formato fasta può essere migliorato
aggiungendo il supporto per altri tag dell'header, così come si può prevedere la
lettura di un insieme di file in input.\\
Se si presentasse la necessità, potrebbe essere aggiunto il supporto di
sequenze e motif scritti in formato diverso da quello fasta per le sequenze e prosite per i pattern.


\begin{appendices}
\chapter{Conversione pattern di prosite in espressione regolare}
\label{prositetoposix}
\begin{verbatim}
int toRegex(const char * iupacStr, char * regex, int size) {
	size = size - 1;
	char * regexPtr = regex;
	char * iup = iupacStr;
	int res;
	int insideClass = 0;

	while (*iup != '\0') {
		int i;
		for (i = 0; i < NUM_CHARS; i++) {
			if (map[i].iupChar == *iup) {
				res = writeRegex(map[i].regexStr, regexPtr,
						(regex + size) - regexPtr);
				regexPtr += res;
				break;
			}
		}

		if (i == NUM_CHARS) {
			regexPtr[0] = *iup;
			regexPtr += 1;
		}

		iup++;
		if (res == -1)
			return MTR_NO_SPACE;
	}
	regexPtr[0] = '\0';
	return MTR_SUCCESS;
}
\end{verbatim}

\end{appendices}

\bibliographystyle{ieeetr}
\bibliography{TesiBioinfo}
\end{document}


