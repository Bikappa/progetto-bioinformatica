/* 
 * File:   FastaFileParser.h
 * Author: luca
 *
 * Created on 3 aprile 2015, 23.21
 */

#ifndef FASTAFILEPARSER_H
#define	FASTAFILEPARSER_H

//global include
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define MAX_NAME_LENGTH 256
#define MAX_HEADER_LENGTH 256
#define MAX_SEQUENCE_LENGTH 100
#define SEQUENCE_HEADER_TAG  '>'
/*
 * describe a fasta file parser
 */
typedef struct ffpStatus {
	//file pointer
	FILE * p;
	char nextHeader[MAX_HEADER_LENGTH];

} FfpStatus;

typedef struct fastaEntry {
	char header[MAX_HEADER_LENGTH];
	char name[MAX_NAME_LENGTH];
	char * sequence;
} FastaEntry;

/*
 *FfpBegin
 *
 *initialize the parser into parserStatus
 */
void FfpBegin(char * filePath, FfpStatus * parserStatus);

/*
 * FfpNextEntry
 *
 * get the next fasta entry given a specific parserStatus
 *
 * return 0 if can't read the entry (file ended) 1 otherwise
 */
int FfpNextEntry(FfpStatus * parserStatus, FastaEntry * entry);

/*
 * FfpEnd
 *
 * realease parserStatus resuorces
 */
void FfpEnd(FfpStatus * parserStatus);

#endif	/* FASTAFILEPARSER_H */

