#include "run_config.h"

void parseRunArguments(int argc, char ** argv, RunConfig * configuration);

//log a running configuration
void printRunConfiguration(RunConfig * configuration) {
	printf(
			"Run configuration:\n\
Sequences input file: %s\n\
Motif input file: %s\n\
Tree output file: %s\n\
Bin tree file: %s\n\
Plot output file: %s\n\
Make plot: %d\n\
Distance metric: %s\n\n",
			configuration->seqFile, configuration->motifsFile,
			configuration->treeFile, configuration->binTreeFile,
			configuration->plotFile, configuration->makePlot,
			configuration->metric);
}

/*
 *getApplicationLaunchPath
 *
 *set the applicationLaunchPath of configuration, relative to the working directory,
 *set  retrieving it but the argv[0] argument if exists
 */
void getApplicationLaunchPath(int argc, char ** argv, RunConfig * configuration) {
	if (argc >= 1) {

		strcpy(configuration->applicationLaunchPath, argv[0]);
	}
	int i;
	for (i = strlen(configuration->applicationLaunchPath) - 1; i >= 0; i--) {
		if (configuration->applicationLaunchPath[i] == '/') {
			configuration->applicationLaunchPath[i + 1] = '\0';
			break;
		}
	}
}
/*
 * setupRunConfiguration
 * initialize program configuration for the current run
 *
 * Perform a command line argument parse for configuration parameter.
 * If some parameters are not configured by command line argument such
 * parameter will be initialized by default values
 */
void setupRunConfiguration(int argc, char ** argv, RunConfig * configuration) {

	getApplicationLaunchPath(argc, argv, configuration);
	printf("Launch path: %s\n\n", configuration->applicationLaunchPath);

	//parse command line arguments
	parseRunArguments(argc, argv, configuration);

	//log section
	printf("Run configuration initialized by command line arguments values\n");
	printRunConfiguration(configuration);
	//end log section

}

/*
 * parseRunArguments
 *
 * parse the command line arguments using the argtable2 librrary. Configuration will be
 * filled with proper values.
 */
void parseRunArguments(int argc, char ** argv, RunConfig * configuration) {
	int nerrors;

	struct arg_lit *shortOutput = arg_lit0("s", NULL,
			"short output, with only tree");
	struct arg_str *metric = arg_str0("m", "metric", NULL,
			"sequences distance metric");
	struct arg_file *binTreeFile = arg_file0(NULL, "newicktree", "<file>",
			"newick format tree output file");
	struct arg_file *treeFile = arg_file0(NULL, "tree", "<file>",
			"tree output file");
	struct arg_file *plotFile = arg_file0(NULL, "plot", "<file>",
			"plot output file");
	struct arg_file *inMotFile = arg_file0(NULL, "motifs", "<file>",
			"motifs input file");
	struct arg_file *inSeqFile = arg_file0(NULL, "sequences", "<file>",
			"sequences input file");
	struct arg_end *end = arg_end(20);
	void *argtable[] = { shortOutput, metric, binTreeFile, treeFile, plotFile,
			inMotFile, inSeqFile, end };

	if (arg_nullcheck(argtable) != 0) {
		printf("error: insufficient memory\n");
		exit(1);
	}

	/*
	 * initialize argtable with default values
	 */
	inMotFile->count = 1;
	inMotFile->filename[0] = malloc(sizeof(char) * (strlen(MOT_INFILE_DEFAULT) +1));
	strcpy(inMotFile->filename[0], MOT_INFILE_DEFAULT);

	inSeqFile->count = 1;
	inSeqFile->filename[0] = malloc(sizeof(char) * (strlen(SEQ_INFILE_DEFAULT) +1));
	strcpy(inSeqFile->filename[0], SEQ_INFILE_DEFAULT);


	binTreeFile->count = 1;
	binTreeFile->filename[0] = malloc(sizeof(char) * (strlen(BINTREE_OUTFILE_DEFAULT) +1));
	strcpy(binTreeFile->filename[0], BINTREE_OUTFILE_DEFAULT);

	treeFile->count = 1;
	treeFile->filename[0] = malloc(sizeof(char) * (strlen(TREE_OUTFILE_DEFAULT) +1));
	strcpy(treeFile->filename[0], TREE_OUTFILE_DEFAULT);

	plotFile->count = 1;
	plotFile->filename[0] = malloc(sizeof(char) * (strlen(PLOT_OUTFILE_DEFAULT) +1));
	strcpy(plotFile->filename[0], PLOT_OUTFILE_DEFAULT);

	shortOutput->count = !MAKE_PLOT_DEFAULT;
	metric->count = 1;
	metric->sval[0] = malloc(sizeof(char)* (strlen(distanceMetricNames[DISTANCE_METRIC_ID_DEFAULT])+1));
	strcpy(metric->sval[0], distanceMetricNames[DISTANCE_METRIC_ID_DEFAULT]);

	printf("Stampo plot: %d", plotFile->count);
	nerrors = arg_parse(argc, argv, argtable);
	printf("Stampo plot: %d", plotFile->count);

	if (nerrors == 0) {
		int i;
		int distanceMisureId = 0;

		for (i = 0; i < N_DIST_MISURES; i++) {
			printf("%makePlot%makePlot\n", metric->sval[0],
					distanceMetricNames[i]);
			if (strcmp(metric->sval[0], distanceMetricNames[i]) == 0) {
				distanceMisureId = i;
				break;
			}
		}
		if (i == N_DIST_MISURES) {
			printf("Fatal error: invalid distance metric");
			exit(1);
		}

		//update configuration values
		strcpy(configuration->motifsFile, inMotFile->filename[0]);
		strcpy(configuration->seqFile, inSeqFile->filename[0]);
		strcpy(configuration->treeFile, treeFile->filename[0]);
		strcpy(configuration->binTreeFile, binTreeFile->filename[0]);
		strcpy(configuration->plotFile, plotFile->filename[0]);
		strcpy(configuration->metric, distanceMetricNames[distanceMisureId]);
		configuration->makePlot = !(shortOutput->count);
	} else {
		printf("Unexpected error parsing command line arguments\n");
		exit(1);
	}

}
