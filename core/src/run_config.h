/*
 * RunConfig.h
 *
 *  Created on: Oct 15, 2015
 *      Author: luca
 */

#ifndef SRC_RUN_CONFIG_H_
#define SRC_RUN_CONFIG_H_

//global headers include
#include <string.h>

//local header includes
#include "distance_metric.h"
#include "argtable/argtable2.h"

//default  configuration values definition
#define SEQ_INFILE_DEFAULT  "sequences.fasta"
#define MOT_INFILE_DEFAULT  "motifs.txt"
#define TREE_OUTFILE_DEFAULT  "outtree.txt"
#define BINTREE_OUTFILE_DEFAULT  "outtree.newick"
#define PLOT_OUTFILE_DEFAULT  "plot.ps"
#define MAKE_PLOT_DEFAULT 1
#define DISTANCE_METRIC_ID_DEFAULT 0


typedef struct runConfig{
		//input sequences file path
		char seqFile [256];

		//input motif file path
		char motifsFile [256];

		//output tree (text representation) file path
		char treeFile [256];

		//output tree file path
		char binTreeFile [256];

		//output plot (graphic representation) file path
		char plotFile [256];

		//application launch path relative to the current working directory
		//is obtained by the argv[0] enviroment argument
		char applicationLaunchPath [256];

		//if plot must be created
		//0 means no create plot
		//otherwise means create plot
		int makePlot;

		//distance metric name
		char metric [20];

} RunConfig;


//log a running configuration
void printRunConfiguration(RunConfig * configuration);


/*
 * initialize program configuration for the current run
 *
 * Perform a command line argument parse for configuration parameter.
 * If some parameters are not configured by command line argument such
 * parameter will be initialized by default values
 */
void setupRunConfiguration(int argc, char** argv, RunConfig * configuration);


#endif /* SRC_RUN_CONFIG_H_ */
