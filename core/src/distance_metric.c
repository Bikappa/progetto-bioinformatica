/*
 * DistanceMetric.c
 *
 *  Created on: Oct 15, 2015
 *      Author: luca
 */
#include "distance_metric.h"

void initDistanceMatrix(double ** distanceMatrix, int nSeqs) {
	int i;
	for (i = 0; i < nSeqs; i++) {
		distanceMatrix[i][i] = 0;
	}
}
/*
 * Perform a chi-square distance evaluation given a frequency matrix
 */
void chiSquareDistance(int * freqMatrix, double ** distanceMatrix, int nSeqs,
		int nMotifs) {
	int i, j, k;

	initDistanceMatrix(distanceMatrix, nSeqs);

	for (i = 0; i < nSeqs; i++) {

		for (j = i + 1; j < nSeqs; j++) {
			float distance = 0;

			for (k = 0; k < nMotifs; k++) {

				// chi square distance

				double avarage = (freqMatrix[i * nMotifs + k]
						+ freqMatrix[j * nMotifs + k]);
				if (avarage == 0) {
					distance += 0;
				} else {
					distance += (float) pow(
							freqMatrix[i * nMotifs + k]
									- freqMatrix[j * nMotifs + k], 2)
							/ (freqMatrix[i * nMotifs + k]
									+ freqMatrix[j * nMotifs + k]);
				}
			}

			//distance = sqrt(distance);
			distance = distance / 2;
			distanceMatrix[i][j] = distanceMatrix[j][i] = distance;
		}
	}
}

/*
 * Perform an euclidean distance evaluation given a frequency matrix
 */
void euclideanDistance(int * freqMatrix, double ** distanceMatrix, int nSeqs,
		int nMotifs) {

	int i, j, k;

	initDistanceMatrix(distanceMatrix, nSeqs);

	for (i = 0; i < nSeqs; i++) {

		for (j = i + 1; j < nSeqs; j++) {
			float distance = 0;

			for (k = 0; k < nMotifs; k++) {

				distance += (float) pow(
						freqMatrix[i * nMotifs + k]
								- freqMatrix[j * nMotifs + k], 2);

			}

			distance = sqrt(distance);
			distanceMatrix[i][j] = distanceMatrix[j][i] = distance;
		}
	}
}

/*
 * Perform a jaccard distance evaluation given a frequency matrix
 */
void jaccardDistance(int * freqMatrix, double ** distanceMatrix, int nSeqs,
		int nMotifs) {
	int i, j, k;

	initDistanceMatrix(distanceMatrix, nSeqs);
	for (i = 0; i < nSeqs; i++) {

		for (j = i + 1; j < nSeqs; j++) {
			int  cardinalitaIntersezione = 0;
			int cardinalitaUnione = 0;

			for (k = 0; k < nMotifs; k++) {

				if ((freqMatrix[i * nMotifs + k] > 0)
						&& (freqMatrix[j * nMotifs + k] > 0))
					cardinalitaIntersezione++;
				if ((freqMatrix[i * nMotifs + k] > 0)
						|| (freqMatrix[j * nMotifs + k] > 0))
					cardinalitaUnione++;

			}

			//distance = distance/2;
			distanceMatrix[i][j] = distanceMatrix[j][i] =1 - (double) cardinalitaIntersezione / (double)cardinalitaUnione;
		}
	}
}

/*
 * Perform an angel distance evaluation given a frequency matrix
 */
void angelDistance(int * freqMatrix, double ** distanceMatrix, int nSeqs,
		int nMotifs) {
	int i, j, k;

	initDistanceMatrix(distanceMatrix, nSeqs);

	float distance = 0;
	for (i = 0; i < nSeqs; i++) {

		for (j = i + 1; j < nSeqs; j++) {
			float prodotto = 0;
			float modulloX = 0;
			float modulloY = 0;

			for (k = 0; k < nMotifs; k++) {
				modulloX += pow(freqMatrix[i * nMotifs + k], 2);
				modulloY += pow(freqMatrix[j * nMotifs + k], 2);
				prodotto += (float) (freqMatrix[i * nMotifs + k]
						* freqMatrix[j * nMotifs + k]);
			}
			modulloX = sqrt(modulloX);
			modulloY = sqrt(modulloY);

			distance = (-1)
					* log((1 + cos(prodotto / (modulloX * modulloY))) / 2);
			//distance = distance/2;
			distanceMatrix[i][j] = distanceMatrix[j][i] = distance;
		}
	}
}

char * distanceMetricNames[N_DIST_MISURES] = { "euclidean", "chi-square",
		"jaccard", "angel" };

int distanceMeasureID(char * measureName) {
	int i = 0;
	for (i = 0; i < N_DIST_MISURES; i++) {
		if (strcmp(measureName, distanceMetricNames[i]) == 0) {
			return i;
		}
	}
	return -1;
}

void (*distanceMeasures[N_DIST_MISURES])(int * freqMatrix,
		double ** distanceMatrix, int nSeqs,
		int nMotifs) = {&euclideanDistance, &chiSquareDistance, &angelDistance, &jaccardDistance
};

