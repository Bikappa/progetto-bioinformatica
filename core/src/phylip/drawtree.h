/*
 * drawtree.h
 *
 *  Created on: Oct 17, 2015
 *      Author: luca
 */

#ifndef SRC_PHYLIP_DRAWTREE_H_
#define SRC_PHYLIP_DRAWTREE_H_
#include "phylip.h"
#include "draw.h"

/* Version 3.696.
   Written by Joseph Felsenstein and Christopher A. Meacham.  Additional code
   written by Sean Lamont, Andrew Keefe, Hisashi Horino, Akiko Fuseki, Doug
   Buxton Michal Palczewski, and James McGill.

   Copyright (c) 1986-2014, Joseph Felsenstein
   All rights reserved.

   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions are met:

   1. Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.

   2. Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.
*/

#ifdef MAC
char* about_message =
  "Drawtree unrooted tree plotting program\r"
  "PHYLIP version 3.696.\r"
  "(c) Copyright 1986-2004 by Joseph Felsenstein.\r"
  "Written by Joseph Felsenstein and Christopher A. Meacham.\r"
  "Additional code written by Sean Lamont, Andrew Keefe, Hisashi Horino,\r"
  "Akiko Fuseki, Doug Buxton and Michal Palczewski.\r"
#endif

//#define JAVADEBUG

#define GAP             0.5
#define MAXITERATIONS   100
#define MINIMUMCHANGE   0.0001

/*When 2 Nodes are on top of each other, this is the max.
  force that's allowed.*/
#ifdef INFINITY
#undef INFINITY
#endif
#define INFINITY        (double) 9999999999.0

typedef enum {fixed, radial, along, middle} labelorient;
FILE        *plotfile;
char        pltfilename[FNMLNGTH];
long        nextnode,  strpwide, strpdeep,
            strptop, strpbottom,  payge, numlines,hpresolution;
double      xmargin, ymargin, topoflabels, rightoflabels, leftoflabels,
             bottomoflabels, ark, maxx, maxy, minx, miny, scale, xscale,
             yscale, xoffset, yoffset, charht, xnow, ynow, xunitspercm,
             yunitspercm, xsize, ysize, xcorner, ycorner,labelheight,
             labelrotation, treeangle, expand, bscale, maxchange;
boolean     canbeplotted, dotmatrix, haslengths,
             uselengths, regular, rotate, empty, rescaled,
             notfirst, improve, nbody, firstscreens, labelavoid;
boolean     pictbold,pictitalic,pictshadow,pictoutline;
boolean javarun;

striptype stripe;
plottertype plotter, oldplotter;
growth grows;
labelorient labeldirec;
node *root, *where;
pointarray nodep;
pointarray treenode;
fonttype font;
enum {  yes, no } penchange, oldpenchange;
char ch,resopts;
char progname [20];
long filesize;
long strpdiv;
double pagex,pagey,paperx,papery,hpmargin,vpmargin;
double *textlength, *firstlet;
double trweight;   /* starting here, needed to make sccs version happy */
boolean goteof;
node *grbg;
winactiontype winaction;

long maxNumOfIter;
struct stackElem
{
  /* This is actually equivalent to a reversed link list; pStackElemBack
     point toward the direction of the bottom of the stack */
  struct stackElem *pStackElemBack;
  node *pNode;
};
typedef struct stackElem stackElemType;


/* function prototypes */
void   initdrawtreenode(node **, node **, node *, long, long, long *,
        long *, initops, pointarray, pointarray, Char *, Char *, FILE *);
void   initialparms(void);
char   showparms(void);
void   getparms(char);
void   getwidth(node *);
void   plrtrans(node *, double, double, double);
void   coordtrav(node *, double *, double *);
double angleof(double , double );
void   polartrav(node *, double, double, double, double, double *,
        double *, double *, double *);
void   tilttrav(node *, double *, double *, double *, double *);

void   leftrightangle(node *, double, double);
void   improvtrav(node *);
void   force_1to1(node *, node *, double *, double *, double);
void   totalForceOnNode(node *, node *, double *, double *, double);
double dotProduct(double, double, double, double );
double capedAngle(double);
double angleBetVectors(double, double, double, double);
double signOfMoment(double, double, double, double);
double forcePerpendicularOnNode(node *, node *, double);
void   polarizeABranch(node *, double *, double *);

void   pushNodeToStack(stackElemType **, node *);
void   popNodeFromStack(stackElemType **, node **);
double medianOfDistance(node *, boolean);
void   leftRightLimits(node *, double *, double *);
void   branchLRHelper(node *, node *, double *, double *);
void   branchLeftRightAngles(node *, double *,  double *);
void   improveNodeAngle(node *, double);
void   improvtravn(node *);
void   coordimprov(double *, double *);
void   calculate(void);
void   rescale(void);
void   user_loop(void);
void setup_environment(int argc, char *argv[]);
void polarize(node *p, double *xx, double *yy);
double vCounterClkwiseU(double Xu, double Yu, double Xv, double Yv);
void drawtree(char* intreename, char* plotfilename, char* plotfileopt, char* fontfilename,
              char* treegrows, int usebranchlengths, char* labeldirecstr, double labelangle,
              double treerotation, double treearc, char* iterationkind, int iterationcount,
              int regularizeangles, int avoidlabeloverlap, int branchrescale,
              double branchscaler, double relcharhgt, double xmarginratio,
              double ymarginratio, int dofinalplot, char* finalplotkind);
/* function prototypes */



#endif /* SRC_PHYLIP_DRAWTREE_H_ */
