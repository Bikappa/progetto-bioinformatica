/* 
 * File:   neighbor.h
 * Author: luca
 *
 * Created on 17 aprile 2015, 10.28
 */

#ifndef NEIGHBOR_H
#define	NEIGHBOR_H
#include <float.h>

#include "phylip.h"
#include "dist.h"
#ifdef	__cplusplus
extern "C" {
#endif
    /* function prototypes */
    void getoptions(void);
    void allocrest(void);
    void doinit(void);
    void inputoptions(void);
    void getinput(void);
    void describe(node *, double);
    void summarize(void);
    void nodelabel(boolean);
    void jointree(void);
    void maketree(void);
    void freerest(void);
    /* function prototypes */

    Char infilename[FNMLNGTH], outfilename[FNMLNGTH], outtreename[FNMLNGTH];
    long nonodes2, outgrno, col, datasets, ith;
    long inseed;
    vector *x;
    intvector *reps;
    boolean jumble, lower, upper, outgropt, replicates, trout,
    printdata, progress, treeprint, mulsets, njoin;
    tree curtree;
    longer seed;
    long *enterorder;

    /* variables for maketree, propagated globally for C version: */
    node **cluster;

#ifdef	__cplusplus
}
#endif

#endif	/* NEIGHBOR_H */

