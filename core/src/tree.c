/*
 * MakeTree.c
 *
 *  Created on: Oct 18, 2015
 *      Author: luca
 */
#include "tree.h"

void makeTree(RunConfig * config, int nSeqs, char ** names,
		double ** distanceMatrix) {

	//code based on phylip drawtree
	int i, j;
	init(0, 0);
	spp = nSeqs;
	int argc = 1;
	char argv[1] = { "tree" };
	init(argc, argv);
	ibmpc = IBMCRT;
	ansi = ANSICRT;
	mulsets = false;
	datasets = 1;
	doinit();
	char fullPath[1000];

	char tempTreePath[1000];
	printf("writing tree files\n");
	sprintf(tempTreePath, config->binTreeFile);

	outfile = fopen(config->treeFile, "w");
	outtree = fopen(config->binTreeFile, "w");
	for (i = 0; i < spp; i++) {
	}
	for (i = 0; i < spp; i++) {
		for (j = 0; j < strlen(names[i]); j++) {

			nayme[i][j] = names[i][j];
		}

		if (strlen(names[i]) < nmlngth) {
			for (j = strlen(names[i]); j < nmlngth; j++) {
				nayme[i][j] = ' ';
			}
		}
	}
	for (i = 0; i < nSeqs; i++) {

		for (j = 0; j < nSeqs; j++) {

			//copy distanceMatrix into phylip x
			x[i][j] = distanceMatrix[i][j];
		}
	}

	ith = 1;
	inputoptions();
	maketree();

	FClose(outfile);
	FClose(outtree);

	freerest();
	freetree(&curtree.nodep, nonodes2 + 1);
	printf("Done\n");
}
