/*
 * DistanceMetric.h
 *
 *  Created on: Oct 15, 2015
 *      Author: luca
 */

#ifndef SRC_DISTANCE_METRIC_H_
#define SRC_DISTANCE_METRIC_H_

#include <math.h>

#define N_DIST_MISURES 4

char * distanceMetricNames[N_DIST_MISURES];

/*
 * Perform a chi-square distance evaluation given a frequency matrix
 */
void chiSquareDistance(int * freqMatrix, double ** distanceMatrix, int nSeqs, int nMotifs);

/*
 * Perform an euclidean distance evaluation given a frequency matrix
 */
void euclideanDistance(int * freqMatrix, double ** distanceMatrix, int nSeqs, int nMotifs);

/*
 * Perform a jaccard distance evaluation given a frequency matrix
 */
void jaccardDistance(int * freqMatrix, double ** distanceMatrix, int nSeqs, int nMotifs);

/*
 * Perform an angel distance evaluation given a frequency matrix
 */
void angelDistance(int * freqMatrix, double ** distanceMatrix, int nSeqs, int nMotifs);

void (*distanceMeasures[N_DIST_MISURES])(int * freqMatrix, double ** distanceMatrix, int nSeqs, int nMotifs);

/*
 * return the distance measure function index given a string representing
 * the measure name
 */
int distanceMeasureID(char * measureName);

#endif /* SRC_DISTANCE_METRIC_H_ */
