/* 
 * File:   MTRConverter.h
 * Author: luca
 *
 * Created on 3 aprile 2015, 10.37
 */

#ifndef MTRCONVERTER_H
#define	MTRCONVERTER_H


#define MTR_SUCCESS 0
#define MTR_NO_SPACE 1

#define MAX_PATTERN_LENGTH 200

/*
 * toRegex
 *
 * convert a iupac motif string into a regular expression string
 *
 */
int toRegex(const char * iupacStr, char * regex, int size);

#endif	/* MTRCONVERTER_H */

