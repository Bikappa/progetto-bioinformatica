/*
 * MakePlot.h
 *
 *  Created on: Oct 17, 2015
 *      Author: luca
 */

#ifndef SRC_PLOT_H_
#define SRC_PLOT_H_

#include "phylip/drawtree.h"
#include "run_config.h"

/*
 * renderPlot
 *
 * render a plot to the file config->plotfile of
 * the tree written in config->binTreeFile
 */
void renderPlot(RunConfig * config);

#endif /* SRC_PLOT_H_ */
