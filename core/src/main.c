/* 
 * File:   main.c
 * Author: Luca Bianconi
 *
 * Created on 3 aprile 2015, 8.58
 *
 *
 * This is the main source file of the tool for construct phylogenetic tree developed for
 * the course "Algoritmi per la bionfiormatica", Master degree in computer science,
 * Università di Padova,
 */

//global headers include
#include <stdio.h>
#include <stdlib.h>
#include <regex.h>
#include <time.h>

#include "distance_metric.h"
//local header include
#include "motif_reader.h"
#include "plot.h"
#include "run_config.h"
#include "sequence_scanner.h"
#include "tree.h"

//define macros
#define die(e) do { fprintf(stderr, "%s\n", e); exit(EXIT_FAILURE); } while (0);

/*
 *benchmark times
 */
clock_t startTime, endTime;
clock_t startReadingMotifTime, endReadingMotifTime;
clock_t startMatchingTime, endMatchingTime;
clock_t startDistanceTime, endDistanceTime;
clock_t startTreeTime, endTreeTime;

//function declaration

/*
 * isPlotFileRequired
 *
 * Return 1 if the plot file should be created given a runconfig
 */
int isPlotFileRequired(RunConfig * config);

/*
 * main
 *
 * the main function
 */
int main(int argc, char ** argv);

int main(int argc, char** argv)
{
	startTime = clock();
	//run configuration for the current application launch
	RunConfig config;
	int j, i, k;
	int nMotifs;
	char ** names;
	int * freqMatrix;
	double ** distanceMatrix;

	//motifs (as regular expression) array
	regex_t * motifs;

	/*
	 * Parse command line parameters
	 */
	setupRunConfiguration(argc, argv, &config);

	/*
	 * collect all motifs from input file
	 */
	printf("Reading motifs from %s\n", config.motifsFile);
	startReadingMotifTime = clock();
	readMotifFile(config.motifsFile, &motifs, &nMotifs);
	endReadingMotifTime = clock();
	printf("Motif count:%d\n\n", nMotifs);

	//list of sequence name
	printf("Reading sequences from %s\n", config.seqFile);
	startMatchingTime = clock();
	int nSeqs = matchAgainstSequences(config.seqFile, motifs, nMotifs, &names,
			&freqMatrix);
	endMatchingTime = clock();
	printf("\nSequences count:%d\n\n", nSeqs);

	printf("Done\n");

	//allocate the matrix for the distances
	distanceMatrix = (double **) malloc(sizeof(double*) * nSeqs);
	for (i = 0; i < nSeqs; i++)
	{
		distanceMatrix[i] = (double *) malloc(sizeof(double) * nSeqs);
	}

	printf("Calculating distance matrix\n");
	int measureId = distanceMeasureID(config.metric);
	startDistanceTime = clock();
	distanceMeasures[measureId](freqMatrix, distanceMatrix, nSeqs, nMotifs);
	endDistanceTime = clock();
	printf("Done\n");

	startTreeTime = clock();
	makeTree(&config, nSeqs, names, distanceMatrix);
	endTreeTime = clock();
	if (isPlotFileRequired(&config))
	{
		renderPlot(&config);
	}

	endTime = clock();

	double totalTime = (double) (endTime - startTime) / CLOCKS_PER_SEC;
	double motifTime = (double) (endReadingMotifTime - startReadingMotifTime)
			/ CLOCKS_PER_SEC;
	double macthingTime = (double) (endMatchingTime - startMatchingTime)
			/ CLOCKS_PER_SEC;
	double distanceTime = (double) (endDistanceTime - startDistanceTime)
			/ CLOCKS_PER_SEC;
	double treeTime = (double) (endTreeTime - startTreeTime) / CLOCKS_PER_SEC;

	printf("Time benchmark:\n");
	printf("Total:\t\t Motif:\t\t Match:\t\t Distance:\t\t Tree:\n");
	printf("%f", totalTime);
	printf("\t%f", motifTime);
	printf("\t%f", macthingTime);
	printf("\t%f", distanceTime);
	printf( "\t%f\n", treeTime);

}

int isPlotFileRequired(RunConfig * runconfig)
{
	return runconfig->makePlot && strlen(runconfig->plotFile) != 0;
}
