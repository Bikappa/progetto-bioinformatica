/*
 * SequenceScanner.h
 *
 *  Created on: Oct 18, 2015
 *      Author: luca
 */

#ifndef SRC_SEQUENCE_SCANNER_H_
#define SRC_SEQUENCE_SCANNER_H_
//global headers include
#include <regex.h>

//local headers include
#include "fasta_file_parser.h"

typedef struct sequenceNodeStruct {

	//fasta formatted sequence entry
	FastaEntry * entry;

	//frequency vector of this sequence
	int * freqVector;

	//size of frequency vector, equals to the number of
	//pattern this sequence is matched against
	int freqVectorSize;

	//next sequence
	struct sequenceNodeStruct * next;
} SequenceNode;

typedef struct matchNodeStruct {

	//starting position of the matched string
	int sPos;

	//end position of the matched string
	int ePos;

	//next node
	struct matchNodeStruct * next;
} MatchNode;

/**
 * matchAgainstSequences
 *
 *match the sequences contained in sequencesFile against the nRegexs regular
 *expressions passed as parameter with regexs array.
 *Create a frequency matrix at the address specified by frequencyMatrixPtr with the matching results and a
 *string array containing sequences name at the address specified by namesPtr
 *
 */
int matchAgainstSequences(char * sequencesFile, regex_t * regexs, int nRegexs,
		char *** namesPtr, int ** frequencyMatrixPtr);

char msgbuf[100];

#endif /* SRC_SEQUENCE_SCANNER_H_ */
