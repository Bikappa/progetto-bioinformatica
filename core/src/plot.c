/*
 * MakePlot.c
 *
 *  Created on: Oct 17, 2015
 *      Author: luca
 */

#include "plot.h"

void renderPlot(RunConfig * config) {
	//code based on phylip drawtree
	initialparms();

	char iterationkind[10] = "nbody";
	char labeldirecstr[10] = "middle";
	char grows[10] = "vertical";
	char fontfile[256];
	strcpy(fontfile, config->applicationLaunchPath);
	strcat(fontfile, "fontfile");

	printf("plotting tree: %s\n", config->binTreeFile);

	drawtree(config->binTreeFile, config->plotFile, "w", fontfile, grows,
			uselengths, labeldirecstr, labelrotation, treeangle, ark,
			iterationkind, 50, regular, labelavoid, 1, bscale, charht, xmargin,
			ymargin, 1, "lw");
	printf("Done\n");

}
