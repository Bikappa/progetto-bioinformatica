#include "fasta_file_parser.h"

/*
 * getNameFromHeader
 *
 * search for organism name into a Fasta header line
 */
void getNameFromHeader(const char * fastaHeader, char * osName);

void completeNextHeader(char * nextHeaderStart, FfpStatus* status);

void completeNextHeader(char * nextHeaderStart, FfpStatus* status)
{
	strcpy(status->nextHeader, nextHeaderStart);
	nextHeaderStart[0] = '\0';

	if (status->nextHeader[strlen(status->nextHeader) - 1] != '\n')
	{
		//finisci leggere header
		if (fgets(status->nextHeader + strlen(status->nextHeader),
		MAX_HEADER_LENGTH - strlen(status->nextHeader), status->p) != NULL)
		{
			if (status->nextHeader[strlen(status->nextHeader) - 1] == '\n')
			{
				status->nextHeader[strlen(status->nextHeader) - 1] = '\0';
			}
		}
	} else
	{
		status->nextHeader[strlen(status->nextHeader) - 1] = '\0';
	}

}

void FfpBegin(char * filePath, FfpStatus * status)
{
	FILE * p = fopen(filePath, "r");
	if (p == NULL)
	{
		printf("error opening file\n");
		exit(1);
	}
	//clear the status
	memset(status, 0, sizeof(FfpStatus));

	//init status file pointer
	status->p = p;
	status->nextHeader[0] = 0;
}

int FfpNextEntry(FfpStatus * status, FastaEntry * entry)
{

	char c;
	char * headerCursor = entry->header;
	int sequenceAcquired;

	/*
	 * Search for entry begin
	 */
	if (strlen(status->nextHeader) == 0)
	{
		/*
		 * nextHeader not read yet
		 */
		if (fgets(entry->header, MAX_HEADER_LENGTH, status->p) != NULL)
		{
			while (entry->header[0] != SEQUENCE_HEADER_TAG)
			{
				fgets(entry->header, MAX_HEADER_LENGTH, status->p);
			}

			if (entry->header[strlen(entry->header) - 1] == '\n')
			{
				entry->header[strlen(entry->header) - 1] = '\0';
			}
		} else
		{
			return 0;
		}
	} else
	{
		//nextHeader known
		strcpy(entry->header, status->nextHeader);
	}

	//sequence name code
	getNameFromHeader(entry->header, entry->name);

	int sequenceBufferSize = MAX_SEQUENCE_LENGTH;

	entry->sequence = malloc(sizeof(char) * sequenceBufferSize);

	entry->sequence[0] = '\0';
	char * seqCursor = entry->sequence;
	sequenceAcquired = 0;
	char seqPart[MAX_SEQUENCE_LENGTH];

	char * nextHeaderStart = NULL;
	while (!sequenceAcquired)
	{

		if (fgets(seqCursor, sequenceBufferSize - strlen(entry->sequence),
				status->p) != NULL)
		{
			//printf("%d %d %d %s\n", strlen(entry->sequence), sequenceBufferSize, sequenceBufferSize - strlen(entry->sequence),seqCursor);
			if (seqCursor[0] == SEQUENCE_HEADER_TAG)
			{
				//sequence end
				//printf("Trovato header\n");
				nextHeaderStart = seqCursor;
				completeNextHeader(nextHeaderStart, status);
				//printf("Nuovo header: %s", status->nextHeader);
				sequenceAcquired = 1;
			} else
			{
				if (seqCursor[strlen(seqCursor) - 1] == '\n')
				{
					seqCursor[strlen(seqCursor) - 1] = '\0';
				}

				if (strlen(entry->sequence) >= sequenceBufferSize - 1)
				{

					//sequence not entirely read so
					//add more space
					sequenceBufferSize += MAX_SEQUENCE_LENGTH;
					//printf("Riallocazione memoria per sequenza: %d\n",sequenceBufferSize);
					entry->sequence = realloc(entry->sequence,
							sizeof(char) * sequenceBufferSize);
					if (NULL == entry->sequence)
					{
						perror("realloc failed");
						exit(-1);
					}
					//printf("fatta: %d\n",sequenceBufferSize);
				}

				seqCursor = entry->sequence + strlen(entry->sequence);
				if(strlen(entry->sequence) == 0){
					sequenceAcquired = 1;
					return 0;
				}
			}
		}else if(strlen(entry->sequence)!=0){

			return 1;
		}else{
			printf("letto eol\n");
			return 0;
		}
	}
	return 1;
}

void FfpEnd(FfpStatus * status)
{
	fclose(status->p);
	free(status);
}

void getNameFromHeader(const char * fastaHeader, char * osName)
{
	//sequence name code
	int index = 2;
	if (strlen(fastaHeader) > 2)
	{
		while (fastaHeader[index] != '\n')
		{
			if ((fastaHeader[index - 3] == 'O')
					&& (fastaHeader[index - 2] == 'S')
					&& (fastaHeader[index - 1] == '='))
			{
				char * nameStart = fastaHeader + index;
				char * nameEnd;

				//search the end position of organism name
				while (strncmp(fastaHeader + index, "GN=", 3) != 0
						&& strncmp(fastaHeader + index, "PE=", 3) != 0
						&& strncmp(fastaHeader + index, "SV=", 3) != 0
						&& fastaHeader[index] != '\0')
				{
					++index;
				}
				nameEnd = fastaHeader + index;
				//turn back to not have spaces in name end
				while (nameEnd[-1] == ' ')
				{
					--nameEnd;
				}

				strncpy(osName, nameStart, nameEnd - nameStart);
				osName[nameEnd - nameStart] = '\0';
				break;
			}
			index++;
		}
	} else
	{
		osName[0] = 0;
	} //end sequence name code
}
