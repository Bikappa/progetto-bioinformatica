#include <stdio.h>
#include <string.h>
#include "motif_to_regex.h"

#define NUM_CHARS 8

struct CharConversion {
	char iupChar;
	char regexStr[9];
} map[NUM_CHARS] = { { '(', "\\{" }, { ')', "\\}" }, { '{', "[^" },
		{ '}', "]" }, { '>', "$" }, { '<', "^" }, { 'x', "." }, { '-', "" } };

int toRegex(const char * iupacStr, char * regex, int size) {
	size = size - 1;
	char * regexPtr = regex;
	char * iup = iupacStr;
	int res;
	int insideClass = 0;

	while (*iup != '\0') {
		int i;
		for (i = 0; i < NUM_CHARS; i++) {
			if (map[i].iupChar == *iup) {
				res = writeRegex(map[i].regexStr, regexPtr,
						(regex + size) - regexPtr);
				regexPtr += res;
				break;
			}
		}

		if (i == NUM_CHARS) {
			regexPtr[0] = *iup;
			regexPtr += 1;
		}

		iup++;
		if (res == -1)
			return MTR_NO_SPACE;
	}
	regexPtr[0] = '\0';
	return MTR_SUCCESS;
}

int writeRegex(char * str, char * regexPtr, int size) {

	int len;
	if ((len = strlen(str)) > size) {
		//not enough space in regex to write str
		return -1;
	} else {
		strcpy(regexPtr, str);
	}

	return len;
}
