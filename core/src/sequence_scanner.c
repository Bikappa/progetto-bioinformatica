/*
 * SequenceScanner.c
 *
 *  Created on: Oct 18, 2015
 *      Author: luca
 */

#include "sequence_scanner.h"

int matchAgainstSequences(char * seqFile, regex_t * regexs, int nMotifs,
		char *** namesPtr, int ** freqMatrixPtr) {

	int nSeqs = 0;
	int i;
	int j;
	FfpStatus status;

	SequenceNode * listHead;

	listHead = malloc(sizeof(SequenceNode));
	SequenceNode * listEnd = NULL;

	FfpBegin(seqFile, &status);

	FastaEntry entry;

	while (FfpNextEntry(&status, &entry) == 1) {
		nSeqs++;

		printf("Analysing sequence %d\n", nSeqs);
		printf("head: %s\nname:%s\nseq:%s\n", entry.header, entry.name, entry.sequence);
		if (listEnd == NULL) {
			//not entries yet
			listEnd = listHead;
			listEnd->freqVector = (int *) malloc(sizeof(int) * nMotifs);
			listEnd->freqVectorSize = nMotifs;
		} else {
			listEnd->next = (SequenceNode *) malloc(sizeof(SequenceNode));
			listEnd = listEnd->next;
			listEnd->freqVector = (int *) malloc(sizeof(int) * nMotifs);
			listEnd->freqVectorSize = nMotifs;
		}

		int j;
		for (j = 0; j < nMotifs; j++) {
			MatchNode * start = NULL;
			int nMatches = findMatches(&regexs[j], entry.sequence, &start, 0);
			listEnd->freqVector[j] = nMatches;

			int cont = 1;
			while (start != NULL) {
				MatchNode * temp = start;
				start = start->next;
				free(temp);
				cont++;
			}
		}

		listEnd->entry = (FastaEntry *) malloc(sizeof(FastaEntry));
		memcpy(listEnd->entry, &entry, sizeof(FastaEntry));
		free(entry.sequence);

	}

	*namesPtr = malloc(sizeof(char *) * nSeqs);
	char ** names = *namesPtr;
	for (i = 0; i < nSeqs; i++)
		names[i] = malloc(sizeof(char) * 40);

	*freqMatrixPtr = (int *) malloc(sizeof(int) * nSeqs * nMotifs);
	int * freqMatrix = *freqMatrixPtr;
	SequenceNode * target = listHead;

	for (i = 0; i < nSeqs; i++) {
		for (j = i; j < nMotifs; j++) {

			freqMatrix[i * nMotifs + j] = target->freqVector[j];
		}
		strcpy(names[i], target->entry->name);

		SequenceNode * t = target;
		target = target->next;
		free(t);
	}

	return nSeqs;
}

int findMatches(regex_t * regex, char * sequence, MatchNode ** matchList,
		int flags) {

	MatchNode * end = *matchList;
	int res = 0;
	int pos = 0;
	int numMatches = 0;

	size_t nmatch = 1;
	regmatch_t pmatch[1];

	while (!res) {
		res = regexec(regex, sequence + pos, nmatch, pmatch, 0);
		if (!res) {
			MatchNode * temp = (MatchNode *) malloc(sizeof(MatchNode));
			temp->next = NULL;
			temp->sPos = pmatch[0].rm_so + pos;
			temp->ePos = pmatch[0].rm_eo + pos;

			if (end == NULL) {
				end = temp;
				*matchList = end;
			} else {
				end->next = temp;
				end = end->next;
			}

			pos += pmatch[0].rm_eo;

			numMatches++;
		} else if (res != REG_NOMATCH) {
			regerror(res, regex, msgbuf, sizeof(msgbuf));
			printf("Regex match failed: %s\n", msgbuf);
			exit(1);
		}
	}
	return numMatches;
}
