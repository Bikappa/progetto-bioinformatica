/*
 * MakeTree.h
 *
 *  Created on: Oct 18, 2015
 *      Author: luca
 */

#ifndef SRC_TREE_H_
#define SRC_TREE_H_
#include "phylip/phylip.h"
#include "phylip/neighbor.h"
#include "run_config.h"

/*
 * makeTree
 *
 * write a tree into files based on config argument.
 * Parameters:
 * 	nSeqs is the number of genetic sequences
 * 	names is the array with the nSeqs names of sequences
 * 	distanceMatrix nSeqs*nSeqs matrix with sequences distance values
 */
void makeTree(RunConfig * config, int nSeqs, char ** names, double ** distanceMatrix);


#endif /* SRC_TREE_H_ */
