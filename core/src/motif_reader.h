/*
 * motif_reader.h
 *
 *  Created on: Oct 21, 2015
 *      Author: luca
 */

#ifndef SRC_MOTIF_READER_H_
#define SRC_MOTIF_READER_H_


//global includes
#include <stdio.h>
#include <regex.h>
#include <stdlib.h>

//local includes
#include "motif_to_regex.h"

/*
 * readMotifFile
 *
 *Read from the file at filePath containing a collection of motifs written in the prosite
 *pattern format described at:
 *http://prosite.expasy.org/scanprosite/scanprosite_doc.html#mo_motifs
 *
 *into the motifRegexs array. Parameter motifCount is the pointer to
 *the number of patterns read from file.
 */
void readMotifFile(char * filePath, regex_t ** motifRegexs, int * motifCount);


#endif /* SRC_MOTIF_READER_H_ */
