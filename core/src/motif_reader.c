/*
 * motif_reader.c
 *
 *  Created on: Oct 21, 2015
 *      Author: luca
 */

#include "motif_reader.h"

//internally used structure
typedef struct motifNode{
	regex_t regex;
	struct motifNode * next;
} MotifNode;

void readMotifFile(char * filePath, regex_t ** motifRegexs, int * nMotifs){
		*nMotifs = 0;
		char c;
		int i = 0;
		char prositePattern[MAX_PATTERN_LENGTH], regexString[MAX_PATTERN_LENGTH];
		MotifNode head;
		MotifNode * targetNode, *temp;
		FILE * motFile;

		motFile = fopen(filePath, "r");

		if (motFile == NULL) {
			perror("Error opening file");
			exit(1);
		}

		*nMotifs = 0;
		while (!feof(motFile)) {
					//consume the prosite pattern identifier string
					//into prositePattern for simplicity... will be discarded
					fscanf(motFile, "%s", prositePattern);

					if (feof(motFile))
						break;

					fscanf(motFile, "%s", prositePattern);
					int res = toRegex(prositePattern, regexString, MAX_PATTERN_LENGTH);

					if (res == MTR_SUCCESS) {
						//compile the regular expression string into a regex structure
											if(*nMotifs == 0){
												targetNode = &head;
											}else{
												targetNode->next = malloc(sizeof(MotifNode));
												targetNode = targetNode->next;
											}
											regcomp(&(targetNode->regex), regexString, 0);

											(*nMotifs)++;
					}else{
						printf("error reading input motifs file\n");
						exit(1);
					}

				}
				fclose(motFile);

				//list to array code
				*motifRegexs = malloc(sizeof(regex_t) * (*nMotifs) );
				temp = NULL;
				targetNode = &head;
				for(i=0; i<*nMotifs; i++){
					(*motifRegexs)[i] = targetNode->regex;
					if(i!=0){
						temp = targetNode;
					}
					targetNode = targetNode->next;

					//clean memory
					free(temp);
				}
}

