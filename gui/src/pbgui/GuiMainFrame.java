package pbgui;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.net.URLDecoder;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import javax.swing.text.BadLocationException;
import javax.swing.text.StyledDocument;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import java.awt.Color;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.GroupLayout;
import javax.swing.UIManager;

public class GuiMainFrame extends JFrame {

	// Variables declaration - do not modify//GEN-BEGIN:variables

	private javax.swing.JPanel jPanel1;
	private javax.swing.JScrollPane jScrollPane1;
	private javax.swing.JSeparator jSeparator1;
	private javax.swing.JTextPane logTextPane;
	private javax.swing.JButton patternsFileChooserButton;
	private javax.swing.JTextField patternsFileChooserField;
	private javax.swing.JLabel patternsFileChooserLabel;
	private javax.swing.JButton plotFileChooserButton;
	private javax.swing.JCheckBox plotFileChooserCheckbox;
	private javax.swing.JTextField plotFileChooserField;
	private javax.swing.JButton runButton;
	private javax.swing.JButton selectMetricButtonChisquare;
	private javax.swing.JButton selectMetricButtonEuclidean;
	private javax.swing.JButton selectMetricButtonJaccard;
	private javax.swing.JLabel selectMetricLabel;
	private javax.swing.JButton sequencesFileChooserButton;
	private javax.swing.JTextField sequencesFileChooserField;
	private javax.swing.JLabel sequencesFileChooserLabel;
	private javax.swing.JButton treeFileChooserButton;
	private javax.swing.JTextField treeFileChooserField;
	private javax.swing.JLabel treeFileChooserLabel;
	final JFileChooser fileChooser;
	
	// End of variables declaration//GEN-END:variables
	private JPanel contentPane;
	private final StyledDocument logText;

	private final String LIN_EXECUTABLE_NAME = "pbcore";

	private static File inputSequencesFile;
	private static File inputPatternsFile;
	private static File nwoutputTreeFile = new File("output_tree.newick");
	private static File outputTreeFile = new File("output_tree.txt");
	private static File outputPlotFile = new File("output_plot.txt");;

	String workingDirectory = Paths.get(".").toAbsolutePath().normalize().toString();
	private String distanceMetrics[] = { "euclidean", "chi-square", "jaccard" };
	private int selectedMetric = 0;

	static String excecutablegDirectory;

	private JButton metricButtons[] = new JButton[3];
	private static boolean outputPlot;
	private boolean executionInProgress = false;
	private JTextField nwtreeFileChooserField;
	private JButton nwtreeFileChooserButton;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Throwable e) {
			e.printStackTrace();
		}
		
		try {
			File executable =
			new File(GuiMainFrame.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath());
			if(executable.isDirectory()){
				excecutablegDirectory = executable.getAbsolutePath();
			}else{
				excecutablegDirectory = executable.getParent();
			}
		} catch (URISyntaxException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		System.out.println("wd: " + excecutablegDirectory.toString());
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GuiMainFrame frame = new GuiMainFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();	
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * 
	 * @throws URISyntaxException
	 * @throws UnsupportedEncodingException
	 */
	public GuiMainFrame() throws URISyntaxException, UnsupportedEncodingException {
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.NORTH);

		jPanel1 = new javax.swing.JPanel();
		sequencesFileChooserButton = new javax.swing.JButton();
		sequencesFileChooserLabel = new javax.swing.JLabel();
		patternsFileChooserLabel = new javax.swing.JLabel();
		patternsFileChooserButton = new javax.swing.JButton();
		patternsFileChooserField = new javax.swing.JTextField();
		sequencesFileChooserField = new javax.swing.JTextField();
		plotFileChooserCheckbox = new javax.swing.JCheckBox();
		plotFileChooserButton = new javax.swing.JButton();
		plotFileChooserField = new javax.swing.JTextField();
		runButton = new javax.swing.JButton();
		selectMetricLabel = new javax.swing.JLabel();
		selectMetricButtonChisquare = new javax.swing.JButton();
		selectMetricButtonJaccard = new javax.swing.JButton();
		selectMetricButtonEuclidean = new javax.swing.JButton();
		jSeparator1 = new javax.swing.JSeparator();
		treeFileChooserLabel = new javax.swing.JLabel();
		treeFileChooserButton = new javax.swing.JButton();
		treeFileChooserField = new javax.swing.JTextField();
		jScrollPane1 = new javax.swing.JScrollPane();
		logTextPane = new javax.swing.JTextPane();

		setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
		setTitle("PBGui");

		sequencesFileChooserButton.setText("Choose File");
		sequencesFileChooserButton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				fileChooserButtonActionPerformed(evt);
			}
		});

		sequencesFileChooserLabel.setText("Sequences");

		patternsFileChooserLabel.setText("Patterns");

		patternsFileChooserButton.setText("Choose File");
		patternsFileChooserButton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				fileChooserButtonActionPerformed(evt);
			}
		});

		patternsFileChooserField.setEditable(false);
		patternsFileChooserField.setBackground(new java.awt.Color(250, 250, 250));

		sequencesFileChooserField.setBackground(new java.awt.Color(250, 250, 250));
		
		outputPlot = plotFileChooserCheckbox.isSelected();
		plotFileChooserCheckbox.setText("Output Plot");
		plotFileChooserCheckbox.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				plotFileChooserCheckboxActionPerformed(evt);
			}
		});

		plotFileChooserButton.setText("Choose File");
		plotFileChooserButton.setEnabled(false);
		plotFileChooserButton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				fileChooserButtonActionPerformed(evt);
			}
		});

		plotFileChooserField.setEditable(false);
		plotFileChooserField.setBackground(new java.awt.Color(250, 250, 250));
		plotFileChooserField.setEnabled(false);

		runButton.setText("Run");
		runButton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				runButtonActionPerformed(evt);
			}
		});

		selectMetricLabel.setText("Select Distance Metric");

		selectMetricButtonChisquare.setText("Chi-square");
		selectMetricButtonChisquare.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				metricButtonActionPerformed(evt);
			}
		});

		selectMetricButtonJaccard.setText("Jaccard");
		selectMetricButtonJaccard.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				metricButtonActionPerformed(evt);
			}
		});

		selectMetricButtonEuclidean.setText("Euclidean");
		selectMetricButtonEuclidean.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				metricButtonActionPerformed(evt);
			}
		});

		treeFileChooserLabel.setText("Output Tree");

		treeFileChooserButton.setText("Choose File");
		treeFileChooserButton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				fileChooserButtonActionPerformed(evt);
			}
		});

		treeFileChooserField.setEditable(false);
		treeFileChooserField.setBackground(new java.awt.Color(250, 250, 250));
		
		JLabel lblOutputNewickTree = new JLabel();
		lblOutputNewickTree.setText("Output Newick Tree");
		
		nwtreeFileChooserButton = new JButton();
		nwtreeFileChooserButton.setText("Choose File");
		nwtreeFileChooserButton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				fileChooserButtonActionPerformed(evt);
			}
		});
		
		nwtreeFileChooserField = new JTextField();
		nwtreeFileChooserField.setEditable(false);
		nwtreeFileChooserField.setBackground(new Color(250, 250, 250));
		
		
		javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
		jPanel1Layout.setHorizontalGroup(
			jPanel1Layout.createParallelGroup(Alignment.TRAILING)
				.addGroup(jPanel1Layout.createSequentialGroup()
					.addGroup(jPanel1Layout.createParallelGroup(Alignment.TRAILING)
						.addGroup(jPanel1Layout.createSequentialGroup()
							.addComponent(treeFileChooserButton)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(treeFileChooserField, 426, 426, 426))
						.addGroup(jPanel1Layout.createSequentialGroup()
							.addComponent(plotFileChooserButton)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(plotFileChooserField, 426, 426, 426))
						.addGroup(jPanel1Layout.createSequentialGroup()
							.addGroup(jPanel1Layout.createParallelGroup(Alignment.TRAILING, false)
								.addComponent(patternsFileChooserButton, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(sequencesFileChooserLabel, Alignment.LEADING)
								.addComponent(patternsFileChooserLabel, Alignment.LEADING)
								.addComponent(sequencesFileChooserButton, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(jPanel1Layout.createParallelGroup(Alignment.LEADING)
								.addComponent(patternsFileChooserField, 432, 432, 432)
								.addComponent(sequencesFileChooserField, 432, 432, 432))))
					.addGap(7))
				.addGroup(jPanel1Layout.createSequentialGroup()
					.addContainerGap(391, Short.MAX_VALUE)
					.addComponent(runButton, GroupLayout.PREFERRED_SIZE, 158, GroupLayout.PREFERRED_SIZE)
					.addContainerGap())
				.addComponent(jSeparator1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
				.addGroup(jPanel1Layout.createSequentialGroup()
					.addGap(0, 0, Short.MAX_VALUE)
					.addComponent(selectMetricButtonEuclidean, GroupLayout.PREFERRED_SIZE, 165, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addComponent(selectMetricButtonChisquare, GroupLayout.PREFERRED_SIZE, 165, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addComponent(selectMetricButtonJaccard, GroupLayout.PREFERRED_SIZE, 165, GroupLayout.PREFERRED_SIZE)
					.addGap(30))
				.addGroup(jPanel1Layout.createSequentialGroup()
					.addGroup(jPanel1Layout.createParallelGroup(Alignment.LEADING)
						.addGroup(jPanel1Layout.createSequentialGroup()
							.addContainerGap()
							.addComponent(plotFileChooserCheckbox))
						.addComponent(treeFileChooserLabel)
						.addComponent(selectMetricLabel))
					.addContainerGap(403, Short.MAX_VALUE))
				.addGroup(jPanel1Layout.createSequentialGroup()
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
					.addComponent(nwtreeFileChooserButton, GroupLayout.PREFERRED_SIZE, 116, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(nwtreeFileChooserField, GroupLayout.PREFERRED_SIZE, 426, GroupLayout.PREFERRED_SIZE)
					.addGap(6))
				.addGroup(Alignment.LEADING, jPanel1Layout.createSequentialGroup()
					.addComponent(lblOutputNewickTree)
					.addContainerGap())
		);
		jPanel1Layout.setVerticalGroup(
			jPanel1Layout.createParallelGroup(Alignment.LEADING)
				.addGroup(jPanel1Layout.createSequentialGroup()
					.addComponent(sequencesFileChooserLabel)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(jPanel1Layout.createParallelGroup(Alignment.BASELINE)
						.addComponent(sequencesFileChooserButton)
						.addComponent(sequencesFileChooserField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(patternsFileChooserLabel)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(jPanel1Layout.createParallelGroup(Alignment.BASELINE)
						.addComponent(patternsFileChooserButton)
						.addComponent(patternsFileChooserField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(36)
					.addComponent(selectMetricLabel)
					.addGap(3)
					.addGroup(jPanel1Layout.createParallelGroup(Alignment.BASELINE)
						.addComponent(selectMetricButtonChisquare, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE)
						.addComponent(selectMetricButtonJaccard, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE)
						.addComponent(selectMetricButtonEuclidean, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED, 50, Short.MAX_VALUE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblOutputNewickTree)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(jPanel1Layout.createParallelGroup(Alignment.BASELINE)
						.addComponent(nwtreeFileChooserButton)
						.addComponent(nwtreeFileChooserField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(9)
					.addComponent(treeFileChooserLabel)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(jPanel1Layout.createParallelGroup(Alignment.BASELINE)
						.addComponent(treeFileChooserButton)
						.addComponent(treeFileChooserField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(4)
					.addComponent(plotFileChooserCheckbox)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(jPanel1Layout.createParallelGroup(Alignment.BASELINE)
						.addComponent(plotFileChooserButton)
						.addComponent(plotFileChooserField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(jSeparator1, GroupLayout.PREFERRED_SIZE, 10, GroupLayout.PREFERRED_SIZE)
					.addGap(4)
					.addComponent(runButton, GroupLayout.PREFERRED_SIZE, 37, GroupLayout.PREFERRED_SIZE))
		);
		jPanel1.setLayout(jPanel1Layout);

		jScrollPane1.setViewportView(logTextPane);

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
		getContentPane().setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(layout.createSequentialGroup().addContainerGap()
						.addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE,
								javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
						.addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 453, Short.MAX_VALUE)
						.addContainerGap()));
		layout.setVerticalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(layout.createSequentialGroup().addContainerGap()
						.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
								.addComponent(jScrollPane1).addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE,
										javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
						.addContainerGap()));

		pack();
		fileChooser = new JFileChooser();
		logText = logTextPane.getStyledDocument();

		if (inputPatternsFile != null) {
			patternsFileChooserField.setText(inputPatternsFile.getAbsolutePath());
		}
		if (inputSequencesFile != null) {
			sequencesFileChooserField.setText(inputSequencesFile.getAbsolutePath());
		}
		if (outputTreeFile != null) {
			treeFileChooserField.setText(outputTreeFile.getAbsolutePath());
		}
		if (nwoutputTreeFile != null) {
			nwtreeFileChooserField.setText(nwoutputTreeFile.getAbsolutePath());
		}
		if (outputPlotFile != null) {
			plotFileChooserField.setText(outputPlotFile.getAbsolutePath());
		}
		metricButtons[0] = selectMetricButtonEuclidean;
		metricButtons[1] = selectMetricButtonChisquare;
		metricButtons[2] = selectMetricButtonJaccard;
		
		updateSelectedMetric();
		
		
		canRun();
	}

	private void fileChooserButtonActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_fileChooserButtonActionPerformed

		fileChooser.setCurrentDirectory(new File(excecutablegDirectory));
		int returnVal = JFileChooser.ABORT;
		if (evt.getSource() == patternsFileChooserButton || evt.getSource() == sequencesFileChooserButton) {
			returnVal = fileChooser.showOpenDialog(this);
			
		} else if (evt.getSource() == treeFileChooserButton
				|| evt.getSource() == plotFileChooserButton
				||  evt.getSource() == nwtreeFileChooserButton) {
			returnVal = fileChooser.showSaveDialog(this);

		}

		try {
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				File file = fileChooser.getSelectedFile();
				String filePath = file.getAbsolutePath();

				if (evt.getSource() == patternsFileChooserButton) {
					logText.insertString(logText.getLength(), "Selected patterns input file:\n" + filePath + "\n\n",
							null);
					inputPatternsFile = new File(filePath);
					patternsFileChooserField.setText(filePath);

				} else if (evt.getSource() == sequencesFileChooserButton) {
					logText.insertString(logText.getLength(), "Selected sequences input file:\n" + filePath + "\n\n",
							null);
					inputSequencesFile = new File(filePath);
					sequencesFileChooserField.setText(filePath);

				} else if (evt.getSource() == treeFileChooserButton) {
					outputTreeFile = new File(filePath);
					treeFileChooserField.setText(filePath);

				}else if (evt.getSource() == nwtreeFileChooserButton) {
					nwoutputTreeFile = new File(filePath);
					nwtreeFileChooserField.setText(filePath);

				} else if (evt.getSource() == plotFileChooserButton) {
					outputPlotFile = new File(filePath);
					plotFileChooserField.setText(filePath);

				}
			}
		} catch (BadLocationException ex) {
			Logger.getLogger(GuiMainFrame.class.getName()).log(Level.SEVERE, null, ex);
		}
		canRun();
		// fileChooser.showOpenDialog(this);
	}// GEN-LAST:event_fileChooserButtonActionPerformed

	private void plotFileChooserCheckboxActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_plotFileChooserCheckboxActionPerformed

		plotFileChooserButton.setEnabled(plotFileChooserCheckbox.isSelected());
		plotFileChooserField.setEnabled(plotFileChooserCheckbox.isSelected());
		outputPlot = plotFileChooserCheckbox.isSelected();
		canRun();

	}// GEN-LAST:event_plotFileChooserCheckboxActionPerformed

	private void metricButtonActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_metricButtonActionPerformed
		Object source = evt.getSource();
		for (int i = 0; i < metricButtons.length; i++) {
			if (source == metricButtons[i]) {
				selectedMetric = i;
				break;
			}
		}

		updateSelectedMetric();
		canRun();
	}// GEN-LAST:event_metricButtonActionPerformed

	private void runButtonActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_runButtonActionPerformed
		runButton.setEnabled(false);
		Thread runningThread;
		runningThread = new Thread(new Runnable() {

			@Override
			public void run() {
				System.out.println(new File(excecutablegDirectory) + File.separator + LIN_EXECUTABLE_NAME);
				try {

					Process process;

					ArrayList<String> cmdArgs = new ArrayList<String>();
					cmdArgs.add( new File(excecutablegDirectory) + File.separator + LIN_EXECUTABLE_NAME);
					cmdArgs.add( "--metric=" + distanceMetrics[selectedMetric]);
					cmdArgs.add( "--newicktree=" + nwoutputTreeFile.getAbsolutePath());
					cmdArgs.add( "--tree=" + outputTreeFile.getAbsolutePath());
					cmdArgs.add( "--motifs=" + inputPatternsFile.getAbsolutePath());
					cmdArgs.add( "--sequences=" + inputSequencesFile.getAbsolutePath());

					if (outputPlot && outputPlotFile != null) {
						System.out.println("output plot required");
						cmdArgs.add(  "--plot" + outputPlotFile.getAbsolutePath());
					}else{
						System.out.println("output plot not required");
						cmdArgs.add("-s");
					}
				

					process = Runtime.getRuntime().exec(cmdArgs.toArray(new String[cmdArgs.size()]));

					
					BufferedReader br = new BufferedReader(new InputStreamReader(process.getInputStream()));

					SwingUtilities.invokeLater(new Runnable() {
						public void run() {
							try {
								logText.insertString(logText.getLength(), "RUN STARTED\n", null);
							} catch (BadLocationException ex) {
								Logger.getLogger(GuiMainFrame.class.getName()).log(Level.SEVERE, null, ex);
							}
						}
					});
					String line;

					while ((line = br.readLine()) != null) {
						System.out.println(line);
						final String ln = line;
						SwingUtilities.invokeLater(new Runnable() {
							public void run() {
								try {
									logText.insertString(logText.getLength(), ln + "\n", null);
								} catch (BadLocationException ex) {
									Logger.getLogger(GuiMainFrame.class.getName()).log(Level.SEVERE, null, ex);
								}

							}
						});

					}
					br.close();
					
					runDrawtree();
					SwingUtilities.invokeLater(new Runnable() {
						public void run() {
							try {
								logText.insertString(logText.getLength(), "RUN FINISHED\n", null);
							} catch (BadLocationException ex) {
								Logger.getLogger(GuiMainFrame.class.getName()).log(Level.SEVERE, null, ex);
							}

						}
					});

					SwingUtilities.invokeLater(new Runnable() {

						public void run() {
							canRun();
						}
					});

					executionInProgress = false;
				} catch (IOException ex) {
					Logger.getLogger(GuiMainFrame.class.getName()).log(Level.SEVERE, null, ex);
				}
			}

		});
		executionInProgress = true;

		runningThread.start();
	}// GEN-LAST:event_runButtonActionPerformed

	protected void runDrawtree() {
		// TODO Auto-generated method stub
		
	}

	private void updateSelectedMetric() {
		for (JButton button : metricButtons) {
			if (button == metricButtons[selectedMetric]) {
				button.setSelected(true);
			} else {
				button.setSelected(false);
			}
		}
		try {
			logText.insertString(logText.getLength(), "Selected metric:\n" + distanceMetrics[selectedMetric] + "\n\n",
					null);
		} catch (BadLocationException ex) {
			Logger.getLogger(GuiMainFrame.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	private boolean canRun() {

		boolean canRun = true;

		canRun = canRun && GuiMainFrame.inputPatternsFile != null && GuiMainFrame.inputPatternsFile.exists();
		canRun = canRun && GuiMainFrame.inputSequencesFile != null && GuiMainFrame.inputSequencesFile.exists();
		canRun = canRun && GuiMainFrame.outputTreeFile != null;

		if (outputPlot) {
			canRun = canRun && GuiMainFrame.outputTreeFile != null;
		}

		canRun = canRun && !this.executionInProgress;
		if (canRun) {
			runButton.setEnabled(true);
		} else {
			runButton.setEnabled(false);
		}
		return canRun;

	}
}


